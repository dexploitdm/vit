<?php
include_once('core/dxd_init.php');
remove_action('wp_head','rsd_link');
remove_action('wp_head','wlwmanifest_link');
remove_action('wp_head','wp_generator');
if (!current_user_can('administrator')):
    show_admin_bar(false);
endif;
add_theme_support('post-thumbnails');

register_nav_menus(array(
    'nav_header_one' => 'Навигация в header 1',
    'nav_footer' => 'Меню в footer',
));
//Отключение стандартного редактора Gutenberg
add_filter('use_block_editor_for_post', '__return_false');

//Форматирование номера для tel:
function telRepl($tel){
    $seven = "+7";
    $number = substr_replace($tel, $seven, 0, 1);
    $result =  str_replace(array( ' ','(', ')','-' ), '', $number);
    echo $result;
}
//Редиректы для каталогов услуг
function taxonomy_rewrite_rule() {
    add_rewrite_rule('dev_site/?$', 'index.php?tax_serv=dev_site', 'top');
    add_rewrite_rule('analitics/?$', 'index.php?tax_serv=analitics', 'top');
    add_rewrite_rule('shops/?$', 'index.php?tax_serv=shops', 'top');
    add_rewrite_rule('question_serv/?$', 'index.php?tax_serv=question_serv', 'top');
    add_rewrite_rule('setting_s/?$', 'index.php?tax_serv=setting_s', 'top');
    add_rewrite_rule('design/?$', 'index.php?tax_serv=design', 'top');
    add_rewrite_rule('webfull/?$', 'index.php?tax_serv=webfull', 'top');
    add_rewrite_rule('crm/?$', 'index.php?tax_serv=crm', 'top');
  }
add_action('init', 'taxonomy_rewrite_rule');



