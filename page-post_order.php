<?php
/*
Template Name: Заявка отправлена
Template Post Type: page
*/
get_header(); ?>
<section class="post_order">
    <div class="layout_post">
        <div class="thanks_post">
            <div class="img_ok">
                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/post_ok.svg">
            </div>
            <div class="info">
                <p class="title">Заявка отправлена</p>
                <p class="desc">Ваша заявка отправлена и уже обрабатывается. Мы свяжемся с вами в ближайшее время</p>
                <a href="<?php echo get_site_url(); ?>/">
                    <button class="b-default"><p>На главную</p></button>
                </a>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>