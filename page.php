<?php
/*
Template Name: Стандартный шаблон
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="top_info type">
        <div class="title_info">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="breadcrumb top">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="breadcrumb_active"><?php the_title(); ?></a>
        </div>
    </div>
    <section class="content_type_page">
        <?php the_content(); ?>
    </section>
<?php endwhile; wp_reset_query(); ?>
    <div class="pagemaps">
        <?php get_template_part( 'components/map'); ?>
    </div>
<?php get_footer(); ?>