//Hosting domain
var link = 'https://dexploitdm.ru/projects/vit.dm/';
//local domain
//var link = 'http://vit.dm';
$('.desktop-nav-list').click(function(){
    var childrenLink = $(this).children()[0];
    childrenLink.click();
});
$('.menu-item').click(function(){
    var childrenSubMenuLink = $(this).children()[0];
    childrenSubMenuLink.click();
});
if(window.innerWidth < 640) {
    $('.sub-menu li').appendTo($('.mainnav-flex-container'));
}
$("#nav_one li.arrownav").click(function() {
    $(this).toggleClass("active");
});
$(".navmob p.title").click(function() {   $('#hamburger-checkbox').click(); });
$('.telefon input').inputmask("8 (999) 999 99 99");
$('.phone_input input').inputmask("8 (999) 999 99 99");
//Slider
$('.slider').each(function() {
    var $this = $(this);
    var $group = $this.find('.slide_group');
    var $slides = $this.find('.slide');
    var bulletArray = [];
    var currentIndex = 0;
    var timeout;

    function move(newIndex) {
        var animateLeft, slideLeft;

        advance();

        if ($group.is(':animated') || currentIndex === newIndex) {
            return;
        }

        bulletArray[currentIndex].removeClass('active');
        bulletArray[newIndex].addClass('active');

        if (newIndex > currentIndex) {
            slideLeft = '100%';
            animateLeft = '-100%';
        } else {
            slideLeft = '-100%';
            animateLeft = '100%';
        }

        $slides.eq(newIndex).css({
            display: 'block',
            left: slideLeft
        });
        $group.animate({
            left: animateLeft
        }, function() {
            $slides.eq(currentIndex).css({
                display: 'none'
            });
            $slides.eq(newIndex).css({
                left: 0
            });
            $group.css({
                left: 0
            });
            currentIndex = newIndex;
        });
    }

    function advance() {
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            if (currentIndex < ($slides.length - 1)) {
                move(currentIndex + 1);
            } else {
                move(0);
            }
        }, 4000);
    }

    $('.next_btn').on('click', function() {
        if (currentIndex < ($slides.length - 1)) {
            move(currentIndex + 1);
        } else {
            move(0);
        }
    });

    $('.previous_btn').on('click', function() {
        if (currentIndex !== 0) {
            move(currentIndex - 1);
        } else {
            move(3);
        }
    });

    $.each($slides, function(index) {
        var $button = $('<a class="slide_btn">&bull;</a>');

        if (index === currentIndex) {
            $button.addClass('active');
        }
        $button.on('click', function() {
            move(index);
        }).appendTo('.slide_buttons');
        bulletArray.push($button);
    });

    advance();
});
//Карусель блока "С нами работают"
//Перенос кнопок листинга карусели
if (document.body.clientWidth <= '600') {
    $( init );
}
function init() {
    $('.customNextBtn').before( $('.customPrevBtn') );
}
var owl = $('.owl-carousel');
//owl.owlCarousel();
$('.customNextBtn').click(function() {
    owl.trigger('next.owl.carousel');
})
$('.customPrevBtn').click(function() {
    owl.trigger('prev.owl.carousel');
})
$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        dotsContainer: '.dotsCont',
        margin: 10,
        responsiveClass: true,
        nav : false,
        pagination : true,
        //autoplay:true, // автопрокрутка
        autoplayTimeout:3000, // задержка в мс
        //loop: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                //loop: false,
                margin: 30
            }
        }
    })
})
//Mansonry
//Gallery works o home (old)
var $grid = $('.grid').masonry({
    itemSelector: '.grid-item',
    percentPosition: true,
    columnWidth: '.grid-sizer'
});
//works new
$(document).ready(function() {
    var $container = $('#works');
    $container.imagesLoaded( function() {
        $container.masonry({
            columnWidth: 10,
            itemSelector: '.box'
        });
        $container.addClass('loaded');
    });
});
//Gallery work on services
var $grid_work = $('.grid_work').masonry({
    itemSelector: '.grid-item',
    percentPosition: true,
    columnWidth: '.grid-sizer'
});
// layout Masonry after each image loads
$grid_work.imagesLoaded().progress( function() {
    $grid_work.masonry();
});
//Portfolio filters
$(document).ready(function(){
    $(".btns_filter button").click(function(){
        if ( $(".btns_filter button").hasClass('active')) {
            $(".btns_filter button").removeClass('active');
        }
        $(this).addClass("active");
    });
    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        if(value == "all") {
            $('.filter').show('1000');
        } else {
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
        }
    });
});
//baguetteBox
window.onload = function() {
    baguetteBox.run('.gridthanks');
};
//Формы
$(document).ready(function(){   
    //Валидация email у формы "Задать вопрос"
    $('#question_form .emailform input').blur(function () {
       var email = $(this).val();
       var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
       if ($(this).val() == '' || re.test(email)) {
           $("#question_form .emailform p").empty()
           if($('#question_form .first input').attr('checked')) {
                $('#question_form .emailform p').append('E-mail *');  
            } else {
                $('#question_form .emailform p').append('E-mail');  
            }
           $('#question_form .emailform .textform').removeClass('error_valid');
       } else {
           $("#question_form .emailform p").empty()
               $('#question_form .emailform p').append('Ошибка!');  
           $('#question_form .emailform .textform').addClass('error_valid');
       }
    });
    //Валидация email у остальных форм
    $('.emailform input').blur(function () {
        var email = $(this).val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
        if ($(this).val() == '' || re.test(email)) {
            $(".emailform p").empty()
            $('.emailform p').append('E-mail *');  
            $('.emailform .textform').removeClass('error_valid');
        } else {
            $(".emailform p").empty()
                $('.emailform p').append('Ошибка!');  
            $('.emailform .textform').addClass('error_valid');
        }
    });
    // Валидация номера формы - не нашли нужную услугу
    $('.phone_serv input').blur(function () {
        var phone = $(this).val();
        var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
        if (res.test(phone)) {
            $(".phone_serv p").empty()
                $('.phone_serv p').append('Номер Вашего телефона');
            $('.phone_serv .body_input').removeClass('error_valid');
        } 
    });
    //Валидация номера у формы "Задать вопрос"
    $('#question_form .telefon input').blur(function () {
        var phone = $(this).val();
        var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
        if ($(this).val() == '' || res.test(phone)) {
            $("#question_form .telefon p").empty()
            if($('#question_form .last input').attr('checked')) {
                    $('#question_form .telefon p').append('Номер Вашего телефона *');
            } else {
                    $('#question_form .telefon p').append('Номер Вашего телефона');
            }
            $('#question_form .telefon .textform').removeClass('error_valid');
        } else {
            $("#question_form .telefon p").empty()
                $('#question_form .telefon p').append('Ошибка!');
            $('#question_form .telefon .textform').addClass('error_valid');
        }
    });
    //Валидация номера у остальных форм
    $('.telefon input').blur(function () {
        var phone = $(this).val();
        var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
        if ($(this).val() == '' || res.test(phone)) {
            $(".telefon p").empty()
            $('.telefon p').append('Номер Вашего телефона *');
            $('.telefon .textform').removeClass('error_valid');
        } else {
            $(".telefon p").empty()
                $('.telefon p').append('Ошибка!');
            $('.telefon .textform').addClass('error_valid');
        }
    });
   function checkPath() {
       var val = $('#textname').val();
       if ($.trim(val)) {
           //если поле заполнили
           $(".textname p").empty()
           $('.textname p').append('Ваше имя *');
           $('.textname .textform').removeClass('error_valid');
       }else{
           $('.textname .textform').addClass('error_valid');
       }
   }
   //По умолчанию - по email
   if($('#question_form .first input').attr('checked', true)) {
       $("#question_form .emailform p").empty()
           $('#question_form .emailform p').append('E-mail *');
   } 
   //Обработчик
    $('.wpcf7-submit').click(function(event) {
        //Проверка при клике submit
       var val = $('#textname').val();
       if ($.trim(val)) {
           $('.textname .textform').removeClass('error_valid');
       }else{
           $(".textname p").empty()
           $('.textname p').append('Ошибка!');
           $('.textname .textform').addClass('error_valid');
           $('#textname').keyup(checkPath);
               checkPath();
       }
        //по email
        var email = $('.emailform input').val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
        if($('.first input').attr('checked')) {
            if( !$('[name="emailform"]').val().trim() || !re.test(email)) { 
                $('.emailform .textform').addClass('error_valid');
                $(".emailform p").empty()
                $('.emailform p').append('Ошибка!');
                return event.preventDefault();
            } 
        } 
       //по телефону
       var phone = $('.telefon input').val();
       var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
       if($('.last input').attr('checked')) {
           if( !$('[name="telefon"]').val().trim() || !res.test(phone)) { 
               $('.telefon .textform').addClass('error_valid');
               $(".telefon p").empty()
               $('.telefon p').append('Ошибка!');
               return event.preventDefault();
           } 
       } 
   });
   //Переключатель (email и телефон)/на форме "Задать вопрос"
   $('.last label').click(function() {  
       $(".emailform p").empty()
           $('.emailform p').append('E-mail');  
       $(".telefon p").empty()
           $('.telefon p').append('Номер Вашего телефона *');
       $('.first input').attr('checked', false);
       $('.last input').attr('checked', true);
       $('.emailform .textform').removeClass('error_valid');
   });
   $('.first label').click(function() {   
       $(".emailform p").empty()
           $('.emailform p').append('E-mail *');
       $(".telefon p").empty()
           $('.telefon p').append('Номер Вашего телефона');
       $('.first input').attr('checked', true);
       $('.last input').attr('checked', false);
       $('.telefon .textform').removeClass('error_valid');
   });
    //***************************//
    //******Остальные формы******//
    //***************************//
    //Валидация email
   $('.email_input input').blur(function () {
       var email = $(this).val();
       var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
       if ($('.email_input input').val() == '' || re.test(email)) {
           $(".email_input p").empty()
               $('.email_input p').append('E-mail');  
           $('.email_input .body_input').removeClass('error_valid');
       } else {
           $(".email_input p").empty()
               $('.email_input p').append('Ошибка!');  
           $('.email_input .body_input').addClass('error_valid');
       }
   });

    //Валидация номера формы - не нашли нужную услугу
   $('.phone_serv input').blur(function () {
       var phone = $(this).val();
       var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
       if (res.test(phone)) {
           $(".phone_serv p").empty()
               $('.phone_serv p').append('Номер Вашего телефона');
           $('.phone_serv .body_input').removeClass('error_valid');
       } else {
           $(".phone_serv p").empty()
               $('.phone_serv p').append('Ошибка!');
           $('.phone_serv .body_input').addClass('error_valid');
       }
   });
   //Валидация имени формы - не нашли нужную услугу
   $('.name_input input').blur(function () {
        var nameServ = $(this).val();
        if ($.trim(nameServ)) {
            $('.name_input .body_input').removeClass('error_valid');
            $(".name_input p").empty();
            $('.name_input p').append('Ваше имя *');
        }else{
            $(".name_input p").empty();
            $('.name_input p').append('Ошибка!');
            $('.name_input .body_input').addClass('error_valid');
        }
    });
    //Валидация сообщения формы - не нашли нужную услугу
    $('.msg_input textarea').blur(function () {
        var nameServ = $(this).val();
        if ($.trim(nameServ)) {
            $('.msg_input .body_input').removeClass('error_valid');
            $(".msg_input p").empty();
            $('.msg_input p').append('Ваше сообщение *');
        }else{
            $(".msg_input p").empty();
            $('.msg_input p').append('Ошибка!');
            $('.msg_input .body_input').addClass('error_valid');
        }
    });
    //Валидация номера остальных форм
    // $('.phones input').blur(function () {
    //     var phone2 = $(this).val();
    //     var res = /^\d[\d\(\)\ -]{4,14}\d$/;
    //     if (res.test(phone2)) {
    //         $(".phones p").empty()
    //             $('.phones p').append('Номер Вашего телефона *');
    //         $('.phones .body_input').removeClass('error_valid');
    //     } else {
    //         $(".phones p").empty()
    //             $('.phones p').append('Ошибка!');
    //         $('.phones .body_input').addClass('error_valid');
    //     }
    // });
});
//Для Contact form 7
$('.textarea-services p').append('Не нашли нужную услугу?');
$('.textname p').append('Ваше имя *');
$('.telefon p').append('Номер Вашего телефона');
$('.emailform p').append('E-mail');
$('.textcompany p').append('Название вашей компании *');
$('.urlsite p').append('Нужна доработка?');
$('.fileform p').append('Есть техническое задание?');
$('.textarea-msg p').append('Ваше сообщение');
$("a.close img").click(function(){
    $('a.close').click();
});
//Ajax post forms
//Form services on home
$(document).ready(function() {
    $('.phone_serv input').blur(function () {
        var phone = $(this).val();
        var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
        if (res.test(phone)) {
            $(".phone_serv p").empty()
                $('.phone_serv p').append('Номер Вашего телефона');
            $('.phone_serv .body_input').removeClass('error_valid');
        } 
    });
    $("#form_services").submit(function(){
        var form = $(this);
        var error = false;
        form.find('.name_input input').each( function(){
            if ($(this).val() == '') {
                $(".name_input p").empty();
                $('.name_input p').append('Ошибка!');
                $('.name_input .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.phone_serv input').each( function(){
            var phone2 = $(this).val();
            var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
            if ($(this).val() == '' || !res.test(phone2)) {
                $(".phone_serv p").empty()
                $('.phone_serv p').append('Ошибка!');
                $('.phone_serv .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.msg_input textarea').each( function(){
            if ($(this).val() == '') {
                $(".msg_input p").empty()
                $('.msg_input p').append('Ошибка!');
                $('.msg_input .body_input').addClass('error_valid');
                error = true;
                console.log('dfgfdg')
            }
        });
        if (!error) {
            var data = form.serialize();
            $.ajax({
                type: 'POST',
                url: "wp-content/themes/vit/components/forms/services.php",
                dataType: 'json',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function(data){
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $('#form_services')[0].reset();
                        location=link + '/post_order';
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                },
                complete: function(data) {
                    form.find('input[type="submit"]').prop('disabled', false);
                }
            });
        }
        return false;
    });

});
//Form consultation
$(document).ready(function() {
    var error = false;
    $('.msg textarea').blur(function () {
        var messages = $(this).val();
        if ($.trim(messages)) {
            $('.msg .body_input').removeClass('error_valid');
            $(".msg p").empty()
                    $('.msg p').append('Ваше сообщение *');
        }
    });
    function checkName() {
        var val = $('.name_oth input').val();
        if ($.trim(val)) {
            //если поле заполнили
            $(".name_oth p").empty()
            $('.name_oth p').append('Ваше имя *');
            $('.name_oth .body_input').removeClass('error_valid');
        }else{
            $('.name_oth .body_input').addClass('error_valid');
        }
    }
    function checkPhone() {
        var val = $('.phones input').val();
        if ($.trim(val)) {
            //если поле заполнили
            $(".phones p").empty()
            $('.phones p').append('Номер Вашего телефона *');
            $('.phones .body_input').removeClass('error_valid');
        }else{
            $(".phones p").empty()
            $('.phones p').append('Ошибка!');
            $('.phones .body_input').addClass('error_valid');
        }
    }
    $("#form_consultation").submit(function(){
        //Проверка имени
        var val = $('.name_oth input').val();
        if ($.trim(val)) {
            $('.name_oth .body_input').removeClass('error_valid');
        }else{
            $(".name_oth p").empty()
            $('.name_oth p').append('Ошибка!');
            $('.name_oth .body_input').addClass('error_valid');
            $('.name_oth').keyup(checkName);
            checkName();
        }
        //Проверка телефона
        var val2 = $('.phones input').val();
        if ($.trim(val2)) {
            $('.phones .body_input').removeClass('error_valid');
        }else{
            $(".phones p").empty()
            $('.phones p').append('Ошибка!');
            $('.phones .body_input').addClass('error_valid');
            $('.phones').keyup(checkPhone);
            checkPhone();
        }
        //Проверка наличие сообщения
        var form = $(this);
        var error = false;
        form.find('textarea').each( function(){
            if ($(this).val() == '') {
                $(".msg p").empty()
                    $('.msg p').append('Ошибка!');
                $('.msg .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.name_oth input').each( function(){
            if ($(this).val() == '') {
                $(".name_oth p").empty()
                $('.name_oth p').append('Ошибка!');
                $('.name_oth .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.phones input').each( function(){
            var phone2 = $(this).val();
            var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
            if ($(this).val() == '' || !res.test(phone2)) {
                $(".phones p").empty()
                $('.phones p').append('Ошибка!');
                $('.phones .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.email_input input').each( function(){
            var email = $(this).val();
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
            if ($(this).val() !== '' && !re.test(email)) {
                $(".email_input p").empty()
                $('.email_input p').append('Ошибка!');
                $('.email_input .body_input').addClass('error_valid');
                error = true;
            }
        });
        if (!error) {
            var data = form.serialize();
            $.ajax({
                type: 'POST',
                url: "wp-content/themes/vit/components/forms/consultation.php",
                dataType: 'json',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function(data){
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $('#form_consultation')[0].reset();
                        location=link + '/post_order';
                     }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                },
                complete: function(data) {
                    form.find('input[type="submit"]').prop('disabled', false);
                }
            });
        }
        return false;
    });
});

//Form on integrate page
$(document).ready(function() {
    $('.msg2 textarea').blur(function () {
        var messages = $(this).val();
        if ($.trim(messages)) {
            $('.msg2 .body_input').removeClass('error_valid');
            $(".msg2 p").empty()
                    $('.msg2 p').append('Ваше сообщение *');
        }else{
            $(".msg2 p").empty()
                    $('.msg2 p').append('Ошибка!');
            $('.msg2 .body_input').addClass('error_valid');
        }
    });
    function checkName() {
        var val = $('.name_oth input').val();
        if ($.trim(val)) {
            //если поле заполнили
            $(".name_oth p").empty()
            $('.name_oth p').append('Ваше имя *');
            $('.name_oth .body_input').removeClass('error_valid');
        }else{
            $('.name_oth .body_input').addClass('error_valid');
        }
    }
    function checkPhone() {
        var val = $('.phones input').val();
        if ($.trim(val)) {
            //если поле заполнили
            $(".phones p").empty()
            $('.phones p').append('Номер Вашего телефона *');
            $('.phones .body_input').removeClass('error_valid');
        }else{
            $(".phones p").empty()
            $('.phones p').append('Ошибка!');
            $('.phones .body_input').addClass('error_valid');
        }
    }
    $("#form_integrate").submit(function(){
        //Проверка имени
        var val = $('.name_oth input').val();
        if ($.trim(val)) {
            $('.name_oth .body_input').removeClass('error_valid');
        }else{
            $(".name_oth p").empty()
            $('.name_oth p').append('Ошибка!');
            $('.name_oth .body_input').addClass('error_valid');
            $('.name_oth').keyup(checkName);
            checkName();
        }
        //Проверка телефона
        var val2 = $('.phones input').val();
        if ($.trim(val2)) {
            $('.phones .body_input').removeClass('error_valid');
        }else{
            $(".phones p").empty()
            $('.phones p').append('Ошибка!');
            $('.phones .body_input').addClass('error_valid');
            $('.phones').keyup(checkPhone);
            checkPhone();
        }
        //Проверка наличие сообщения
        var form = $(this);
        var error = false;
        form.find('textarea').each( function(){
            if ($(this).val() == '') {
                $(".msg p").empty()
                    $('.msg p').append('Ошибка!');
                $('.msg .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.name_oth input').each( function(){
            if ($(this).val() == '') {
                $(".name_oth p").empty()
                $('.name_oth p').append('Ошибка!');
                $('.name_oth .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.phones input').each( function(){
            var phone2 = $(this).val();
            var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
            if ($(this).val() == '' || !res.test(phone2)) {
                $(".phones p").empty()
                $('.phones p').append('Ошибка!');
                $('.phones .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.email_input input').each( function(){
            var email = $(this).val();
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
            if ($(this).val() !== '' && !re.test(email)) {
                $(".email_input p").empty()
                $('.email_input p').append('Ошибка!');
                $('.email_input .body_input').addClass('error_valid');
                error = true;
            }
        });
        if (!error) {
            var data = form.serialize();
            $.ajax({
                type: 'POST',
                url: link + "wp-content/themes/vit/components/forms/integrate.php",
                dataType: 'json',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function(data){
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $('#form_integrate')[0].reset();
                        location=link + '/post_order';
                     }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                },
                complete: function(data) {
                    form.find('input[type="submit"]').prop('disabled', false);
                }
            });
        }
        return false;
    });
});
//Форма "Оставить заявку" на странице портфолио
$(document).ready(function() {
    $('.order_msg textarea').blur(function () {
        var messages = $(this).val();
        if ($.trim(messages)) {
            $('.order_msg .body_input').removeClass('error_valid');
            $(".order_msg p").empty()
            $('.order_msg p').append('Ваше сообщение *');
        }else{
            $(".order_msg p").empty()
            $('.order_msg p').append('Ошибка!');
            $('.order_msg .body_input').addClass('error_valid');
        }
    });
    $('.order_name input').blur(function () {
        var val_name = $(this).val();
        if ($.trim(val_name)) {
            $('.order_name .body_input').removeClass('error_valid');
            $(".order_name p").empty()
            $('.order_name p').append('Ваше имя *');
        }else{
            $(".order_name p").empty()
            $('.order_name p').append('Ошибка!');
            $('.order_name .body_input').addClass('error_valid');
        }
    });
    $('.order_phone input').blur(function () {
        var phone_order = $(this).val();
        var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
        if ($(this).val() == '' || res.test(phone_order)) {
            $(".order_phone p").empty()
            $('.order_phone p').append('Номер Вашего телефона *');
            $('.order_phone .body_input').removeClass('error_valid');
        } else {
            $(".order_phone p").empty()
            $('.order_phone p').append('Ошибка!');
            $('.order_phone .body_input').addClass('error_valid');
        }
    });
    $("#form_order").submit(function(){
        //Проверка наличие сообщения
        var form = $(this);
        var error = false;
        form.find('.order_msg textarea').each( function(){
            if ($(this).val() == '') {
                $(".order_msg p").empty()
                $('.order_msg p').append('Ошибка!');
                $('.order_msg .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.order_name input').each( function(){
            if ($(this).val() == '') {
                $(".order_name p").empty()
                $('.order_name p').append('Ошибка!');
                $('.order_name .body_input').addClass('error_valid');
                error = true;
            }
        });
        form.find('.order_phone input').each( function(){
            var phone_order = $(this).val();
            var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
            if ($(this).val() == '' || !res.test(phone_order)) {
                $(".order_phone p").empty()
                $('.order_phone p').append('Ошибка!');
                $('.order_phone .body_input').addClass('error_valid');
                error = true;
            }
        });
        if (!error) {
            var data = form.serialize();
            $.ajax({
                type: 'POST',
                url: link + "wp-content/themes/vit/components/forms/order.php",
                dataType: 'json',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function(data){
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $('#form_order')[0].reset();
                        //location=link + '/post_order';
                        $.magnificPopup.close();
                        $.magnificPopup.open({
                            callbacks: {
                                open: function() {
                                    $('body').addClass('is_blurmob');
                                },
                                close: function() {
                                    $('body').removeClass('is_blurmob');
                                }
                            },
                            items: {
                                src: '#order_post',            
                            },
                            type: 'inline',
                            removalDelay: 300,
                            mainClass: 'my-mfp-zoom-in'
                        });
                     }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                },
                complete: function(data) {
                    form.find('input[type="submit"]').prop('disabled', false);
                }
            });
        }
        return false;
    });
});
//Others
$(document).ready(function() {
    $('.wpcf7-list-item').click(
        function(){
            if($(this).find(":checkbox").attr('checked')) {
                $(this).find(":checkbox").attr("checked", false);
            } else { 
                $(this).find(":checkbox").attr("checked","checked");
            }
    });
});
//Кнопка наверх    
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if(height > 250){
    $('#to-top-button').addClass('show');
    } else{
    /*Если меньше 100px удаляем класс для header*/
    $('#to-top-button').removeClass('show');
    }
});
(function($) {
    $(function() {
        $('#to-top-button').click(function() {
            $('html, body').animate({scrollTop: 0},500);
            return false;
        })
    })
})(jQuery)