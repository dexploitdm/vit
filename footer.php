</main>
<footer>
<?php 
$tel = get_option('phone_v'); 
?>
<section class="footer">
    <div class="layout_footer">
        <div class="body_logo">
            <div class="logo">
                <a href="<?php echo get_site_url(); ?>">
                    <div class="logo_c">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/logo_c.svg">
                    </div>
                    <div class="logo_s">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/logo_s.svg">
                    </div>
                </a>
            </div>
            <div class="brand">
                <a href="<?php echo get_site_url(); ?>">
                    <div class="text_a"><p>Вятка IT</p></div>
                    <div class="text_b"><p class="blue">web</p><p>-студия</p> </div>
                </a>
            </div>
        </div>
        <div class="nav_footer">
            <?php wp_nav_menu(array(
                'theme_location'  => 'nav_footer',
                'container' => '',
                'menu_class' => '',
            )); ?>
        </div>
        <div class="phone_footer">
            <a href="tel:<?php telRepl($tel); ?>"><?php echo get_option('phone_v'); ?></a>
            <a class="mail" href="mailto:<?php echo get_option('email_v'); ?>"><?php echo get_option('email_v'); ?></a>
        </div>
    </div>
    <div class="bootom_f">
        <p>© 2015-<?php echo date('Y'); ?> Веб-студия «Вятка IT». Все права защищены.</p>
    </div>
</section>
</footer>
<button id="to-top-button" class="to-top-button">
  Вверх
</button>
<?php get_template_part( 'components/modals'); ?>
<script>
 $("#nav_one .menu li ul").append('<li class="elemhide"><a href="<?php echo get_site_url(); ?>/online_request/">Онлайн заявка</a></li>');
 $("#nav_one .menu li ul").append('<li class="elemhide"><a href="<?php echo get_site_url(); ?>/contacts/">Контакты</a></li>');
 $("li.service18 ul").prepend('<li><a href="<?php echo get_site_url(); ?>/shops/">Интернет-магазин</a></li>');
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '678' == event.detail.contactFormId ) {
        $.magnificPopup.close();
        $.magnificPopup.open({
            callbacks: {
                open: function() {
                    $('body').addClass('is_blurmob');
                },
                close: function() {
                    $('body').removeClass('is_blurmob');
                }
            },
	        items: {
	            src: '#question_post',            
            },
	        type: 'inline',
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });
    }
    if ( '435' == event.detail.contactFormId ) {
        location="<?php echo get_site_url(); ?>/post_order";
    }
}, false );
$('.question_link').magnificPopup({
    callbacks: {
        open: function() {
            $('body').addClass('is_blurmob');
        },
        close: function() {
            $('body').removeClass('is_blurmob');
        }
    },
    closeBtnInside:true
});
$('.order_link').magnificPopup({
    callbacks: {
        open: function() {
            $('body').addClass('is_blurmob');
        },
        close: function() {
            $('body').removeClass('is_blurmob');
        }
    },
    closeBtnInside:true
});
</script>
<?php if( is_home() ): ?>
<script>
$(".brand_logo a").removeAttr('href');
$(".body_logo a").removeAttr('href');
</script>
<?php endif; ?>
<?php wp_footer(); ?>
<!--<script src="https://code.jquery.com/jquery-2.1.4.js"></script>-->
<script src="<?php  echo get_template_directory_uri() ?>/assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/assets/js/baguetteBox.min.js" async"></script>
<script src="<?php  echo get_template_directory_uri() ?>/assets/js/jquery.inputmask.min.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/assets/js/main.js"></script>
</body>
</html>