<?php
/*
Template Name: Благодарности
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
<div class="top_info">
    <div class="title_info">
        <h1><?php the_title(); ?></h1>
    </div>
    <div class="breadcrumb">
        <a href="<?php echo get_site_url(); ?>/">Главная</a>
        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
        <a class="breadcrumb_active">Благодарности</a>
    </div>
</div>
<div class="links_btns">
    <a href="<?php echo get_site_url(); ?>/reviews"><button class="btn_h">Отзывы</button></a>
    <a href="https://vk.com/topic-34687056_31455682" target="_blank"><button class="btn_vk"><glyph class="vk_icon"></glyph>Оставить отзыв</button></a>
</div>
<section class="content_thanks">
    <?php $the_thanks = new WP_Query(array('post_type' => 'the_thanks', 'order' => 'ASC')) ?>
    <?php if ($the_thanks->have_posts() ): ?>
        <div class="layout_thanks">
            <div class="gridthanks gallery">
                <?php while ($the_thanks->have_posts()) : $the_thanks->the_post(); ?>
                    <div class="single_thank">
                        <a href="<?php the_post_thumbnail_url(); ?>">
                            <img src="<?php the_post_thumbnail_url(); ?>">
                        </a>
                        <div class="title_thank"><p><?php the_title(); ?></p></div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>
</section>
<?php endwhile; wp_reset_query(); ?>
<div class="pagemaps">
    <?php get_template_part( 'components/map'); ?>
</div>
<?php get_footer(); ?>