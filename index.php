<?php get_header(); ?>
<?php get_template_part( 'components/slider'); ?>
<section id="content">
        <div class="advantages">
            <div id="grid_advantages">
                <div class="single_adv">
                    <div class="icon_adv">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/interview.svg">
                    </div>
                    <div class="title_adv"><p>Клиентский сервис</p></div>
                    <div class="desc_adv"><p>Человеческое отношение, профессионализм и скорость реагирования удивляют даже конкурентов</p></div>
                </div>
                <div class="single_adv">
                    <div class="icon_adv">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/chat.svg">
                    </div>
                    <div class="title_adv"><p>Нам не пофиг</p></div>
                    <div class="desc_adv"><p>Сотрудникам компании не безразлично, что будет с вашим проектом. Даже при небольшом бюджете</p></div>
                </div>
                <div class="single_adv">
                    <div class="icon_adv">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/group.svg">
                    </div>
                    <div class="title_adv"><p>Кропотливая работа<br> над проектом</p></div>
                    <div class="desc_adv"><p>На каждом этапе происходит длительная, кропотливая работа,
                            не лишенная рутинных процессов</p></div>
                </div>
                <div class="single_adv">
                    <div class="icon_adv">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/slippers.svg">
                    </div>
                    <div class="title_adv"><p>Работаем в тапочках</p></div>
                    <div class="desc_adv"><p>Процесс работы должен быть комфортным, что позволяет быть более сконцентрированными над поставленными задачами</p></div>
                </div>
            </div>
        </div>
    </section>
<section class="services_home">
    <div class="title_service_h">
        <h2>Наши услуги</h2>
    </div>
    <div id="grid_services_home">
    <?php
    $terms = get_terms(
        'tax_serv', array(
        'order'    => 'DESC',
        'hide_empty' => 0,
        'number' => 6,
    ) );
    $taxonomy = 'tax_serv';
    foreach( $terms as $term ) {
        $args = array(
            'post_type' => 'serv',
            'tax_serv' => $term->slug
        );
        $query = new WP_Query( $args ); ?>
        <?php
        $term_link = get_term_link($term->term_id, $taxonomy);
        $id_term_img = $term->term_id; ?>
        <div class="single_services_h">
            <a href="<?php echo $term_link; ?>" class="link_services">
                <div class="t_b_l">
                    <div class="lines_service_h t_b_r">
                        <div class="l_t_b">
                            <div class="r_t_b">
                                <div class="layout_service_h">
                                    <div class="icon_service">
                                        <div class="bg_standart">
                                            <img src="<?php the_field('term_image_def', $taxonomy . '_' . $id_term_img); ?>" alt="" />
                                        </div>
                                        <div class="bg_before">
                                            <img src="<?php the_field('term_image_after', $taxonomy . '_' . $id_term_img); ?>" alt="" />
                                        </div>
                                    </div>
                                    <div class="title_s_h">
                                        <p><?php echo $term->name; ?></p>
                                    </div>
                                    <div class="desc_s_h">
                                        <p><?php echo $term->description; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php  wp_reset_postdata(); } ?>
    </div>
</section>
<section class="form_home">
        <div class="layout_form_h">
            <div class="body_form_h">
                <div class="desc_form_h">
                    <div class="title_form_h">
                        <p>Не нашли нужную услугу?</p>
                    </div>
                    <div class="content_form_h">
                        <p>Дюшан предложил новое понимание того, что такое искусство и чем оно может стать. Конечно, оно по-прежнему.</p>
                    </div>
                </div>
                <div class="form_h">
                    <div>
                        <form id="form_services">
                            <div class="sector_form">
                                <div class="name_input">
                                    <div class="body_input">
                                        <p>Ваше имя *</p>
                                        <input type="text" name="name" placeholder="Как Вас зовут?">
                                    </div>
                                </div>
                                <div class="phone_input phone_serv">
                                    <div class="body_input">
                                        <p>Номер Вашего телефона *</p>
                                        <input id="phone_usl" name="phonezinput" type="text" placeholder="8 (___) ___ __ __" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="msg_input">
                                    <div class="body_input">
                                        <p>Ваше сообщение *</p>
                                        <textarea name="message" placeholder="Напишите что Вас интересует"></textarea>
                                    </div>
                                </div>
                                <div class="btn_form">
                                    <button class="btn_h" href="/">Оставить заявку</button>
                                </div>
                                <div class="personal_data">
                                    <p>Нажимая на кнопку Вы даете согласие на обработку <a href="<?php echo get_site_url(); ?>/privacy_policy" target="_blank">персональных данных</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="bg_from_img">
                        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/other/ne_nashli_uslugu.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="work-home-mobile">
    <?php get_template_part( 'components/services_home/mobile'); ?>
</div>
<div class="work-home-desktop">
    <?php get_template_part( 'components/services_home/desktop'); ?>
</div>



<section class="block">
    <div class="title_h">
        <h2>С нами работают</h2>
    </div>
        <?php $work_with_us = new WP_Query(array('post_type' => 'work_with_us', 'order' => 'ASC')) ?>
        <?php if ($work_with_us->have_posts() ): ?>
            <div class="work_carousel">
                <div class="owl-carousel owl-theme">
                    <?php while ($work_with_us->have_posts()) : $work_with_us->the_post(); ?>
                        <div class="item">
                            <div class="layout_item">
                                <div class="icon_company"><img src="<?php the_post_thumbnail_url(); ?>"></div>
                                <div class="desc_company">
                                    <a href="<?php echo get_post_meta($post->ID, 'btnonelink', true); ?>"><?php the_title(); ?></a>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="oldy">
                    <button class="customPrevBtn"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/arow_pagl.svg"></button>
                    <div class="dotsCont desktop">
                        <div></div>
                    </div>
                    <button class="customNextBtn"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/arow_pagr.svg"></button>
                </div>
            </div>
        <?php endif; ?>
</section>
<section class="reviews_block">
    <div class="title_h">
        <h2>Отзывы клиентов</h2>
    </div>
    <?php get_template_part( 'components/reviews/vk_reviews_home'); ?>
    
    <div class="all_reviews">
        <a href="<?php echo get_site_url(); ?>/reviews">
            <button class="rev"><p>Показать еще</p></button>
        </a>
    </div>
</section>
<section class="block cons">
        <div class="request_consult">
            <div class="consult_content">
                <div class="body_cons">
                    <div class="title">
                        <p>Оставьте заявку на консультацию</p>
                    </div>
                    <div class="desc"><p>Оставьте свои данные и наш специалист свяжется с Вами</p></div>
                    <form class="form_consult" id="form_consultation">
                        <div class="left_block">
                            <div class="left_f_m">
                                <div class="name_input name_oth">
                                    <div class="body_input">
                                        <p>Ваше имя *</p>
                                        <input type="text" name="name" placeholder="Как Вас зовут?">
                                    </div>
                                </div>
                                <div class="phone_input phones">
                                    <div class="body_input">
                                        <p>Номер Вашего телефона *</p>
                                        <input id="phone_consult" name="phone" type="text" placeholder="8 (___) ___ __ __" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="email_input">
                                    <div class="body_input">
                                        <p>Электронная почта</p>
                                        <input type="text" name="email" placeholder="example@mail.ru">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right_block">
                            <div class="msg_input msg">
                                <div class="body_input">
                                    <p>Ваше сообщение *</p>
                                    <textarea name="message" placeholder="Напишите что Вас интересует"></textarea>
                                </div>
                            </div>
                            <div class="btn_form">
                                <button class="btn_h" href="/">Оставить заявку</button>
                            </div>
                        </div>
                        <div class="personal_data">
                            <p>Нажимая на кнопку Вы даете согласие на обработку <a href="<?php echo get_site_url(); ?>/privacy_policy" target="_blank">персональных данных</a></p>
                        </div>
                    </form>
                </div>
                <div class="consult_image">
                    <img src="<?php  echo get_template_directory_uri() ?>/assets/img/other/interview.png">
                </div>
            </div>
        </div>
    </section>
<?php get_template_part( 'components/map'); ?>
<?php get_footer(); ?>