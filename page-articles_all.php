<?php
/*
Template Name: Наш блог
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="top_info type">
        <div class="title_info">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="breadcrumb top">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="breadcrumb_active"><?php the_title(); ?></a>
        </div>
    </div>
<?php endwhile; wp_reset_query(); ?>
<section class="page_jobs">
<?php $blog = new WP_Query(array('post_type' => 'blog', 'order' => 'ASC')) ?>
<?php if ($blog->have_posts() ): ?>
    <div class="layout_jobs">
        <div class="gridjobs">
    <?php while ($blog->have_posts()) : $blog->the_post(); ?>
            <div class="single_job">
                <div class="job_title">
                    <p><?php the_title(); ?></p>
                    <span class="date_job"><?php echo get_the_date('j F Y'); ?></span>
                </div>
                <div class="job_desc">
                    <?php the_excerpt(); ?>
                </div>
                <div class="job_btn">
                    <a href="<?php the_permalink(); ?>">
                        <button class="job"><p>Подробнее</p></button>
                    </a>
                </div>
            </div>
    <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
</section>
<?php get_footer(); ?>