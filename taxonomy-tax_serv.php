<?php get_header(); ?>
    <!--    Шаблон категории  "Обычный"-->
<?php
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$taxonomy = 'tax_serv';
$term = get_term( $term_id, $taxonomy );
$name_title = $term->name;
$id_filed = $term->term_id; ?>
    <section id="banner" style=" background-image:
    <?php if( get_field('cover_ban', $taxonomy . '_' . $id_filed) ): ?>
            url(<?php the_field('cover_ban', $taxonomy . '_' . $id_filed); ?>);
    <?php else: ?>
            url(<?php  echo get_template_directory_uri() ?>/assets/img/banners/landing_page.png);
    <?php endif; ?>">
        <div class="layout_banner">
            <div class="content_banner">
                <h1><?php the_field('titile_dev_banner', $taxonomy . '_' . $id_filed); ?></h1>
                <div class="breadcrumb">
                    <a href="<?php echo get_site_url(); ?>/">Главная</a>
                    <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
                    <a class="breadcrumb_active"><?php echo $name_title; ?></a>
                </div>
                <div class="desc_banner">
                    <?php the_field('desc_dev_banner', $taxonomy . '_' . $id_filed); ?>
                </div>
            </div>
            <div class="btns_banner">
                <a href="#order_form" class="order_button order_link">Заказать разработку</a>
                <a href="<?php echo get_site_url(); ?>/reviews" class="reviews_button">Отзывы клиентов</a>
            </div>
        </div>
    </section>
    <section class="block_service other_pad">
        <div class="title_h">
            <h2>Стоимость услуг</h2>
        </div>
        <div id="grid_services_home" class="integration">
            <?php
            $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
            $args = array(
                'post_type' => 'serv',
                'tax_serv' => $term->slug,
                'order' => 'ASC'
            );
            $query = new WP_Query( $args ); ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <div class="single_services">
                    <div class="t_b_l">
                        <div class="lines_service_h t_b_r">
                            <div class="l_t_b">
                                <div class="r_t_b">
                                    <div class="layout_service_h services_pad">
                                        <div class="title_s_h">
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="desc_s_h">
                                                <p><?php the_field('meta_description_services'); ?></p>
                                            </div>
                                        </a>
                                        <div class="price_service">
                                            <p><?php the_field('price_service'); ?></p>
                                        </div>
                                        <div class="btns_service_s">
                                            <input type="hidden" value="<?php the_title(); ?>" id="title_input<?php the_ID(); ?>">
                                            <a href="#order_form" class="order_dev order_link" onclick="titleFunction<?php the_ID(); ?>()">Заказать разработку</a>
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="img_arrow_s">
                                                    <glyph class="arrow_icon"/></glyph>
                                                </div>
                                            </a>
                                            <script>
                                            function titleFunction<?php the_ID(); ?>(){
                                                var val = document.getElementById('title_input<?php the_ID(); ?>').value;
                                                document.getElementById('result_title').value=val;
                                            }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </section>
<?php get_template_part( 'components/map'); ?>
<?php if( get_field('seoblock_serv', $taxonomy . '_' . $id_filed) ): ?>
    <section class="content_type_page seoblock">
        <?php the_field('seoblock_serv', $taxonomy . '_' . $id_filed); ?>
    </section>
<?php else :?><?php endif; ?>
<?php get_footer(); ?>