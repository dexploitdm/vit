<?php
/*
Template Name: Наша команда
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
<div class="bg_team" style=" background-image: url(<?php the_post_thumbnail_url(); ?>);">
    <div class="top_info">
        <div class="title_info">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="breadcrumb">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="active">Наша команда</a>
        </div>
        <div class="desc_team">
            <?php the_field('desc_team'); ?>
        </div>
    </div>
</div>
<?php endwhile; wp_reset_query(); ?>
<section class="our_team">
    <?php $our_team = new WP_Query(array('post_type' => 'our_team', 'order' => 'ASC')) ?>
    <?php if ($our_team->have_posts() ): ?>
    <div class="layout_teams">
        <div class="teams">
            <?php while ($our_team->have_posts()) : $our_team->the_post(); ?>
                <div class="people">
                    <div class="line_bl">
                        <div class="line_br">
                            <div class="line_l">
                                <div class="line_r">
                                    <div class="layout_team">
                                        <div class="bg_cover"
                                             style="background-image: url(<?php the_field('cover_card_team'); ?>);">
                                            <div class="body_card">
                                                <div class="bg_avatar" style="background-image: url(<?php the_field('avatar_team'); ?>);"></div>
                                                <div class="name_team"><p><?php the_title(); ?></p></div>
                                                <div class="status_people"><p><?php the_field('status_work_team'); ?></p></div>
                                                <div class="social_team">
                                                    <?php get_template_part( 'components/social'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
</section>
<section class="team_content">
    <div class="our_fotos"><p>Наши фотки</p></div>
    <?php while( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; wp_reset_query(); ?>
</section>
<style>
    #bwg_container1_0 #bwg_container2_0 .bwg-container-0 .bwg-item {
        justify-content: flex-start;
        max-width: 300px;
    }
    .team_content .bwg-container-0 .bwg-item > a {
        margin-right: 15px !important;
        margin-left: 15px;
        margin-bottom: 30px !important;
    }
    #bwg_container1_0 #bwg_container2_0 .bwg-container-0 {
        width: 1200px;
    }
</style>
<?php get_footer(); ?>