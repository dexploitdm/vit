<?php
/*
Template Name: Онлайн заявка
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="top_info">
        <div class="title_info">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="breadcrumb">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="breadcrumb_active">Онлайн заявка</a>
        </div>
    </div>
    <section class="online_order">
        <div class="desc_order">
            <?php the_content(); ?>
        </div>
        <?php
        if ( function_exists('dynamic_sidebar') )
            dynamic_sidebar('homepage-sidebar');
        ?>
        <div class="proccess_data"><p>Нажимая на кнопку Вы даете согласие на обработку <a href="<?php echo get_site_url(); ?>/privacy_policy" target="_blank">персональных данных</a></p></div>
    </section>
<script>
    $(document).ready(function(){
        $(".online_order .emailform p").empty()
        $('.online_order .emailform p').append('E-mail *');
        $(".online_order .telefon p").empty()
        $('.online_order .telefon p').append('Номер Вашего телефона *');
        $(".online_order .textarea-msg  p").empty()
        $('.online_order .textarea-msg p').append('Ваше сообщение *');
    });
    function checkName() {
       var val = $('.online_order #textname').val();
       if ($.trim(val)) {
           //если поле заполнили
           $(".online_order .textname p").empty()
           $('.online_order .textname p').append('Ваше имя *');
           $('.online_order .textname .textform').removeClass('error_valid');
       }else{
           $('.online_order .textname .textform').addClass('error_valid');
       }
    }
    function checkCompany() {
       var val = $('#textcompany').val();
       if ($.trim(val)) {
           //если поле заполнили
           $(".textcompany p").empty()
           $('.textcompany p').append('Название вашей компании *');
           $('.textcompany .textform').removeClass('error_valid');
       }else{
           $('.textcompany .textform').addClass('error_valid');
       }
    }
    function checkMsg() {
       var val = $('.online_order .textarea-msg textarea').val();
       if ($.trim(val)) {
           //если поле заполнили
           $(".online_order .textarea-msg p").empty()
           $('.online_order .textarea-msg p').append('Ваше сообщение *');
           $('.online_order .textarea-msg .textform').removeClass('error_valid');
       }else{
           $('.online_order .textarea-msg .textform').addClass('error_valid');
       }
    }
    function checkPhone() {
       var phone = $('.online_order .telefon input').val();
       var res = /8 \(\d{3}\) \d{3}\ \d{2}\ \d{2}/;
        if( !$('.online_order .telefon input').val().trim() || !res.test(phone)) { 
            $('.online_order .telefon .textform').addClass('error_valid');
            $(".online_order .telefon p").empty()
            $('.online_order .telefon p').append('Ошибка!');
        } else {
            $(".online_order .telefon p").empty()
            $('.online_order .telefon p').append('Номер Вашего телефона *');
            $('.online_order .telefon .textform').removeClass('error_valid');
        }
    }

    //Обработчик
    $('.wpcf7-submit').click(function(event) {
        //Имя
        var val = $('.online_order #textname').val();
        if ($.trim(val)) {
            $('.online_order .textname .textform').removeClass('error_valid');
        }else{
            $(".online_order .textname p").empty()
            $('.online_order .textname p').append('Ошибка!');
            $('.online_order .textname .textform').addClass('error_valid');
            $('.online_order #textname').keyup(checkName);
            checkName();
        }
        //Название компании
        var valcompany = $('#textcompany').val();
        if ($.trim(valcompany)) {
            $('.textcompany .textform').removeClass('error_valid');
        }else{
            $(".textcompany p").empty()
            $('.textcompany p').append('Ошибка!');
            $('.textcompany .textform').addClass('error_valid');
            $('#textcompany').keyup(checkCompany);
            checkCompany();
        }
        //Сообщение
        var valcompany = $('.online_order .textarea-msg textarea').val();
        if ($.trim(valcompany)) {
            $('.online_order .textarea-msg .textform').removeClass('error_valid');
        }else{
            $(".online_order .textarea-msg p").empty()
            $('.online_order .textarea-msg p').append('Ошибка!');
            $('.online_order .textarea-msg .textform').addClass('error_valid');
            $('.online_order .textarea-msg textarea').keyup(checkMsg);
            checkMsg();
        }
        //по телефону
        var val = $('.online_order .telefon input').val();
        if ($.trim(val)) {
            $('.online_order .telefon .textform').removeClass('error_valid');
        }else{
            $(".online_order .telefon p").empty()
            $('.online_order .telefon p').append('Ошибка!');
            $('.online_order .telefon .textform').addClass('error_valid');
            $('.online_order .telefon input').keyup(checkPhone);
            checkPhone();
        }
        //по email
        var email = $('.online_order .emailform input').val();
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
            if( !$('.online_order [name="emailform"]').val().trim() || !re.test(email)) { 
                $('.online_order .emailform .textform').addClass('error_valid');
                $(".online_order .emailform p").empty()
                $('.online_order .emailform p').append('Ошибка!');
            return event.preventDefault();
        } 
   });
</script>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>