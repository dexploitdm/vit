<?php
function cptui_register_my_cpts_serv() {

    /**
     * Post Type: Услуги.
     */

    $labels = array(
        "name" => __( "Услуги", "Vyatka IT" ),
        "singular_name" => __( "Услуги", "Vyatka IT" ),
        "menu_name" => __( "Услуги", "Vyatka IT" ),
        "all_items" => __( "Все услуги", "Vyatka IT" ),
        "add_new" => __( "Добавить услугу", "Vyatka IT" ),
        "add_new_item" => __( "Добавление новой услуги", "Vyatka IT" ),
        "edit_item" => __( "Редактирование услуги", "Vyatka IT" ),
        "new_item" => __( "Новая услуга", "Vyatka IT" ),
        "view_item" => __( "Посмотреть", "Vyatka IT" ),
        "view_items" => __( "Посмотреть услуги", "Vyatka IT" ),
        "search_items" => __( "Поиск услуг", "Vyatka IT" ),
        "not_found" => __( "Не найдено", "Vyatka IT" ),
        "not_found_in_trash" => __( "Не найдено в корзине", "Vyatka IT" ),
        "parent_item_colon" => __( "Родитель услуги", "Vyatka IT" ),
        "parent_item_colon" => __( "Родитель услуги", "Vyatka IT" ),
    );

    $args = array(
        "label" => __( "Услуги", "Vyatka IT" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        // "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => true,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "serv", "with_front" => false ),
        'has_archive' => 'serv',
        "query_var" => true,
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/technical.png',
        "supports" => array( "title", "editor", "thumbnail", "excerpt", "custom-fields", "page-attributes", "post-formats" ),
    );

    register_post_type( "serv", $args );
}

add_action( 'init', 'cptui_register_my_cpts_serv' );

function cptui_register_my_taxes() {

    /**
     * Taxonomy: Каталоги.
     */

    $labels = array(
        "name" => __( "Каталоги", "Vyatka IT" ),
        "singular_name" => __( "Каталоги", "Vyatka IT" ),
        "menu_name" => __( "Каталоги услуг", "Vyatka IT" ),
        "all_items" => __( "Все каталоги", "Vyatka IT" ),
        "edit_item" => __( "Редактировать каталог", "Vyatka IT" ),
        "view_item" => __( "Посмотреть каталог", "Vyatka IT" ),
        "update_item" => __( "Обновить имя каталога", "Vyatka IT" ),
        "add_new_item" => __( "Добавить новый каталог", "Vyatka IT" ),
        "new_item_name" => __( "Имя нового каталога", "Vyatka IT" ),
        "parent_item" => __( "Родитель каталога", "Vyatka IT" ),
        "search_items" => __( "Поиск каталогов", "Vyatka IT" ),
        "not_found" => __( "Не найдено", "Vyatka IT" ),
    );

    $args = array(
        "label" => __( "Каталоги", "Vyatka IT" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
        'rewrite' => array(
            'slug' => 'tax_serv', // Текст в ЧПУ. По умолчанию: название таксономии.
            'with_front' => false, // Позволяет ссылку добавить к базовому URL.
            'hierarchical' => true, // Использовать (true) или не использовать (false) древовидную структуру ссылок. По умолчанию: false.
            'ep_mask' => EP_NONE, // Перезаписывает конечное значение таксономии. По умолчанию: EP_NONE.
        ),
        // 'rewrite' => array('slug'=>'serv', 'hierarchical'=>true),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "tax_serv",
        "rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		);
	register_taxonomy( "tax_serv", array( "serv" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes' );


// Удаление slug произвольной записи serv
function true_post_type_rewrite() {
    global $wp_rewrite;
    // параметры add_rewrite_tag('%название_тега%', '%маска_символов%', '%url_параметр%')
    $wp_rewrite->add_rewrite_tag("%serv%", '([^/]+)', "serv=");
    $wp_rewrite->add_permastruct('serv', '%serv%' );
}
add_action( 'init', 'true_post_type_rewrite');
 
function true_rewrite_conflicts( $request ) {
    if(!is_admin())
        $request['post_type'] = array('serv', 'post', 'page', 'blog', 'jobs'); // перечисляем типы записей с подобной структурой пермалинков
    return $request;
}
add_filter( 'request',  'true_rewrite_conflicts' );

// Удаление slug таксономии sentence/tax_serv
function taxonomy_link( $link, $term, $taxonomy ) {
    if ( $taxonomy !== 'tax_serv' )
        return $link;
    return str_replace( 'tax_serv/', '', $link );
}
add_filter( 'term_link', 'taxonomy_link', 10, 3 );

/*
 * Фильтрация
 */
function danilin_add_taxonomy_filters() {
    global $typenow;
// таксономии
    $taxonomies = array('tax_serv');
// пользовательский тип данных
    if( $typenow == 'serv' ){
        foreach ($taxonomies as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            if(count($terms) > 0) {
                echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
                echo "<option value=''>$tax_name</option>";
                foreach ($terms as $term) {
                    echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
                }
                echo "</select>";
            }
        }
    }
}
add_action( 'restrict_manage_posts', 'danilin_add_taxonomy_filters' );