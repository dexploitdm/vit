<?php
function sliders(){
    register_post_type('sliders', array(
        'public' => true,
        'supports' => array('title','editor','thumbnail'),
        'menu_position' => 7,
        'function' => 'my_sliders',
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/slider.png',
        'labels' => array(
            'name' => 'Слайдеры',
            'all_items' => 'Все слайды',

        ),
    ));
}
function sliders_meta_box() {
    add_meta_box(
        'sliders_meta_box',
        'Информация',
        'show_sliders_meta_box',
        'sliders',
        'normal',
        'high');
}
$addsliders_meta_fields = array(
    array(
        'label' => 'Текст первой кнопки',
        'desc'  => 'Если текста нету, то кнопка будет отсутствовать',
        'id'    => 'btnonetext',
        'type'  => 'text'
    ),
    array(
        'label' => 'Ссылка первой кнопки',
        'desc'  => '',
        'id'    => 'btnonelink',
        'type'  => 'text'
    ),
    array(
        'label' => 'Текст второй кнопки',
        'desc'  => 'Если текста нету, то кнопка будет отсутствовать',
        'id'    => 'btntwotext',
        'type'  => 'text'
    ),
    array(
        'label' => 'Ссылка второй кнопки',
        'desc'  => '',
        'id'    => 'btntwolink',
        'type'  => 'text'
    )
);
function show_sliders_meta_box() {
    global $addsliders_meta_fields;
    global $post;
    echo '<input type="hidden" name="custom_meta_box_nonce_sliders" value="'.wp_create_nonce(basename(__FILE__)).'" />';


    echo '<table class="form-table">';
    foreach ($addsliders_meta_fields as $field) {
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
        switch($field['type']) {
            case 'text':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
					        <br /><span class="description">'.$field['desc'].'</span>';
                break;
        }
        echo '</td></tr>';
    }
    echo '</table>';
}
function save_sliders_meta_fields($post_id) {
    global $addsliders_meta_fields;

    if (!wp_verify_nonce($_POST['custom_meta_box_nonce_sliders'], basename(__FILE__)))
        return $post_id;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    if ('red_book' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($addsliders_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
function xelly_remove_sub_menu_items() {
    remove_submenu_page( 'edit.php?post_type=sliders', 'post-new.php?post_type=sliders' );
}
add_action('init','sliders');
add_action('save_post', 'save_sliders_meta_fields');
add_action('add_meta_boxes', 'sliders_meta_box');