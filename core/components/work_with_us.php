<?php
function work_with_us(){
    register_post_type('work_with_us', array(
        'public' => true,
        'supports' => array('title','editor','thumbnail'),
        'menu_position' => 7,
        'function' => 'my_work_with_us',
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/partner.png',
        'labels' => array(
            'name' => 'С нами работают',
            'all_items' => 'Все партнеры',

        ),
    ));
}
function work_with_us_meta_box() {
    add_meta_box(
        'work_with_us_meta_box',
        'Информация',
        'show_work_with_us_meta_box',
        'work_with_us',
        'normal',
        'high');
}
$addwork_with_us_meta_fields = array(
    array(
        'label' => 'Ссылка',
        'desc'  => '',
        'id'    => 'btnonelink',
        'type'  => 'text'
    )
);
function show_work_with_us_meta_box() {
    global $addwork_with_us_meta_fields;
    global $post;
    echo '<input type="hidden" name="custom_meta_box_nonce_work_with_us" value="'.wp_create_nonce(basename(__FILE__)).'" />';


    echo '<table class="form-table">';
    foreach ($addwork_with_us_meta_fields as $field) {
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
        switch($field['type']) {
            case 'text':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
					        <br /><span class="description">'.$field['desc'].'</span>';
                break;
        }
        echo '</td></tr>';
    }
    echo '</table>';
}


function save_work_with_us_meta_fields($post_id) {
    global $addwork_with_us_meta_fields;

    if (!wp_verify_nonce($_POST['custom_meta_box_nonce_work_with_us'], basename(__FILE__)))
        return $post_id;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    if ('red_book' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($addwork_with_us_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
add_action('init','work_with_us');
add_action('save_post', 'save_work_with_us_meta_fields');
add_action('add_meta_boxes', 'work_with_us_meta_box');