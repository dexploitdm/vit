<?php
function our_team(){
    register_post_type('our_team', array(
        'public' => true,
        'supports' => array('title'),
        'menu_position' => 7,
        'function' => 'my_teams',
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/team.png',
        'labels' => array(
            'name' => 'Наша команда',
            'all_items' => 'Вся команда',
        ),
    ));
}
add_action('init','our_team');
