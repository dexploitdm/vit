<?php
function change_post_menu_label() {
    global $menu, $submenu;
    $menu[5][0] = 'Портфолио';
    $submenu['edit.php'][5][0] = 'Портфолио';
    $submenu['edit.php'][10][0] = 'Добавить работу';
    $submenu['edit.php'][16][0] = 'Метки';
    echo '';
}
add_action( 'admin_menu', 'change_post_menu_label' );
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Портфолио';
    $labels->singular_name = 'Портфолио';
    $labels->add_new = 'Добавить работу';
    $labels->add_new_item = 'Добавить работу';
    $labels->edit_item = 'Редактировать работу';
    $labels->new_item = 'Добавить работу';
    $labels->view_item = 'Посмотреть работу';
    $labels->search_items = 'Найти работу';
    $labels->not_found = 'Не найдено';
    $labels->not_found_in_trash = 'Корзина пуста';
}
add_action( 'init', 'change_post_object_label' );