<?php
function seo_block(){
    register_post_type('seo_block', array(
        'public' => true,
        'supports' => array('title','editor'),
        'menu_position' => 99,
        'function' => 'my_seo_block',
        //'menu_icon'           =>   get_template_directory_uri() .  '/core/img/slider.png',
        'labels' => array(
            'name' => 'Сео блок',
            'all_items' => 'Вся информация',

        ),
    ));
}
add_action('init','seo_block');