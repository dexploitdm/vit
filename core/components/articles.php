<?php
function cptui_register_my_cpts_blog() {
    /**
     * Post Type: Наш блог.
     */
    $labels = array(
        "name" => __( "Наш блог", "Vyatka IT" ),
        "singular_name" => __( "Наш блог", "Vyatka IT" ),
        "all_items" => __( "Все записи", "Vyatka IT" ),
        "add_new" => __( "Добавить", "Vyatka IT" ),
        "add_new_item" => __( "Добавить новою запись", "Vyatka IT" ),
        "edit_item" => __( "Редактирование записи", "Vyatka IT" ),
        "new_item" => __( "Добавить", "Vyatka IT" ),
        "view_item" => __( "Посмотреть", "Vyatka IT" ),
        "view_items" => __( "Посмотреть записи", "Vyatka IT" ),
        "search_items" => __( "Поиск записей", "Vyatka IT" ),
        "not_found" => __( "Не найдено", "Vyatka IT" ),
        "not_found_in_trash" => __( "Не найдено в корзине", "Vyatka IT" ),
        "parent_item_colon" => __( "Родитель записи", "Vyatka IT" ),
        "parent_item_colon" => __( "Родитель записи", "Vyatka IT" ),
    );

    $args = array(
        "label" => __( "Наш блог", "Vyatka IT" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/article.png',
        "rewrite" => array( "slug" => "blog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "excerpt", "custom-fields", "page-attributes", "post-formats" ),
    );

    register_post_type( "blog", $args );
}

add_action( 'init', 'cptui_register_my_cpts_blog' );
