<?php
function jobs(){
    register_post_type('jobs', array(
        'public' => true,
        'supports' => array('title','editor','thumbnail','excerpt'),
        'menu_position' => 7,
        'function' => 'my_jobs',
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/job.png',
        "hierarchical" => false,
        // 'has_archive' => true,
        "rewrite" => array( "slug" => "jobs", "with_front" => false ),
        "query_var" => true,
        'has_archive' => 'jobs',
        'labels' => array(
            'name' => 'Вакансии',
            'all_items' => 'Все вакансии',

        ),
    ));
}
add_action('init','jobs'); 