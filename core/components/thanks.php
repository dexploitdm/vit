<?php
function the_thanks(){
    register_post_type('the_thanks', array(
        'public' => true,
        'supports' => array('title','thumbnail'),
        'menu_position' => 7,
        'function' => 'my_teams',
        'menu_icon'           =>   get_template_directory_uri() .  '/core/img/thank.png',
        'labels' => array(
            'name' => 'Благодарности',
            'all_items' => 'Все благодарности',
        ),
    ));
}
add_action('init','the_thanks');
