<?php
define( 'ACF_LITE', false );
include_once('advanced-custom-fields/acf.php');
//Убираем теги <p> у изображений
include_once('components/non_tag_p-image.php');
//Слайдер
include_once('components/sliders.php');
//Перевод ссылок
include_once('components/rus-to-lat.php');
//Оключаем Emoji
include_once('components/unlock_emoji.php');
//Блок "С нами работают"
include_once('components/work_with_us.php');
// Боевая версия
include_once('components/service_full.php');
//Исполькозование каткгориизаписей для портфолио
include_once('components/portfolio.php');
//Widgets
include_once('widgets/forms.php');
//Наша команда
include_once('components/our_team.php');
//Настройки
include_once('components/setting.php');
//Благодарности
include_once('components/thanks.php');
//Вакансии
include_once('components/jobs.php');
//Редиректы
include_once('components/wp-simple-301-redirects.php');
//Блог
include_once('components/articles.php');
//Seo
include_once('components/seo.php');







