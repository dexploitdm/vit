<?php
/*
Template Name: Портфолио
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="top_info">
        <div class="title_info">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="breadcrumb">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="breadcrumb_active"><?php the_title(); ?></a>
        </div>
    </div>
    <div class="portfolio_work">
        <?php
        $idObj = get_category_by_slug('portfolio');
        $parent_id = $idObj->term_id;
        $sub_cats = get_categories( array(
            'parent' => $parent_id, 'hide_empty' => 0) );
        if( $sub_cats ): ?>
            <style>
                <?php  foreach( $sub_cats as $cat ): ?>
                #tab<?php echo $cat->term_id?>:checked ~ #content-tab<?php echo $cat->term_id?>,
                <?php endforeach; ?>
                #tab1:checked ~ #content-tab1 {display: block;}</style>
        <?php endif; ?>
        <div class="tabs">
            <input id="tab1" type="radio" name="tabs" checked>
            <label style="margin-right: 3px" for="tab1" title="Wordpress"><p>Все</p></label>
            <?php
            $idObj = get_category_by_slug('portfolio');
            $parent_id = $idObj->term_id;
            $sub_cats = get_categories( array(
                'parent' => $parent_id, 'hide_empty' => 0,'order'=> 'DESC') );
            if( $sub_cats ): ?>
                <?php  foreach( $sub_cats as $cat ): ?>
                    <input id="tab<?php echo $cat->term_id?>" type="radio" name="tabs">
                    <label for="tab<?php echo $cat->term_id?>" title="<?php echo $cat->name?>">
                        <p><?php echo $cat->name?></p>
                    </label>
                <?php endforeach; ?>
            <?php endif; ?>
            <section id="content-tab1">
                <!--            Вывод всех-->
                <div class="container_filter">
                    <div class="body_filter">
                        <?php $idObj = get_category_by_slug('portfolio');
                        $all = $idObj->term_id;
                        $wpb_all_query = new WP_Query(array(
                            'cat' => $all,
                            'post_type'=>'post',
                            'post_status'=>'publish',
                        )); ?>
                        <?php if ( $wpb_all_query->have_posts() ) : ?>
                            <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                                <div class="gallery_product">
                                <div class="bg_work_cover" style="background-image:
                                    <?php if( get_field('cover_page_work') ): ?>
                                            url(<?php echo get_field('cover_page_work'); ?>);
                                    <?php else: ?>
                                            url(<?php echo get_the_post_thumbnail_url(); ?>);
                                    <?php endif; ?>">
                                    <div class="bg_desc_work">
                                            <div class="body_desc_work">
                                                <div class="title_w_h"><p><?php the_title(); ?></p></div>
                                                <div class="desc_w_h"><p><?php echo the_excerpt(); ?></p></div>
                                                <a href="<?php the_permalink(); ?>">
                                                    <button>Смотреть<glyph class="arrow_icon_s"></glyph></button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        <?php else : ?>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            <?php
            $idObj = get_category_by_slug('portfolio');
            $parent_id = $idObj->term_id;
            $sub_cats = get_categories( array(
                'parent' => $parent_id, 'hide_empty' => 0) );
            if( $sub_cats ): ?>
                <?php  foreach( $sub_cats as $cat ): ?>
                    <section id="content-tab<?php echo $cat->term_id?>">
                        <div class="container_filter">
                            <div class="body_filter">
                                <?php
                                $child_parent = $cat->term_id;
                                $sub_cats = get_categories( array(
                                    'child_of' => $child_parent, 'hide_empty' => 0,'order'=> 'ASC') );
                                if( $sub_cats ): ?>
                                    <div class="btns_filter">
                                        <button class="btn btn-default filter-button active" data-filter="all"><p>Все</p></button>
                                        <?php  foreach( $sub_cats as $cat ): ?>
                                            <button class="btn btn-default filter-button" data-filter="<?php echo $cat->slug; ?>">
                                                <p><?php echo $cat->name?></p>
                                            </button>
                                        <?php endforeach; ?>
                                    </div>
                                    <?php  foreach( $sub_cats as $cat ): ?>
                                        <?php
                                        $myposts = get_posts( array(
                                            'category'    => $cat->term_id,
                                            'orderby'     => 'post_date',
                                            'order'       => 'DESC',
                                        ) );
                                        global $post;
                                        foreach($myposts as $post): ?>
                                            <?php  setup_postdata($post); ?>
                                            <div class="gallery_product
                            filter <?php foreach((get_the_category()) as $category): ?>
                                <?php echo $category->category_nicename ?>
                            <?php endforeach; ?>
                            ">
                                            <div class="bg_work_cover" style="background-image:
                                                    <?php if( get_field('cover_page_work') ): ?>
                                                            url(<?php echo get_field('cover_page_work'); ?>);
                                                    <?php else: ?>
                                                            url(<?php echo get_the_post_thumbnail_url(); ?>);
                                                    <?php endif; ?>">
                                                    <div class="bg_desc_work">
                                                        <div class="body_desc_work">
                                                            <div class="title_w_h"><p><?php echo get_the_title(); ?></p></div>
                                                            <div class="desc_w_h"><p><?php echo get_the_excerpt(); ?></p></div>
                                                            <a href="<?php echo get_the_permalink(); ?>">
                                                                <button>Смотреть<glyph class="arrow_icon_s"></glyph></button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                        <?php  wp_reset_postdata();?>
                                    <?php endforeach;   wp_reset_postdata();?>
                                <?php  else: ?>
                                    <!--                Вывод вывод работ без фильтра (если подкатегорий нет)-->
                                    <?php
                                    $myposts = get_posts( array(
                                        'category'    => $cat->term_id,
                                        'orderby'     => 'post_date',
                                        'order'       => 'DESC',
                                    ) );
                                    global $post;
                                    foreach($myposts as $post): ?>
                                        <?php  setup_postdata($post); ?>
                                        <div class="gallery_product non_filter">
                                            <div class="bg_work_cover" style="background-image:
                                                <?php if( get_field('cover_page_work') ): ?>
                                                        url(<?php echo get_field('cover_page_work'); ?>);
                                                <?php else: ?>
                                                        url(<?php echo get_the_post_thumbnail_url(); ?>);
                                                <?php endif; ?>">
                                                <div class="bg_desc_work">
                                                    <div class="body_desc_work">
                                                        <div class="title_w_h"><p><?php echo get_the_title(); ?></p></div>
                                                        <div class="desc_w_h"><p><?php echo get_the_excerpt(); ?></p></div>
                                                        <a href="<?php echo get_the_permalink(); ?>">
                                                            <button>Смотреть<glyph class="arrow_icon_s"></glyph></button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php  wp_reset_postdata();?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </section>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
<div style="clear: both"></div>
<?php get_template_part( 'components/map'); ?>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>