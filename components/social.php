<?php if( in_array( 'vk', get_field('social_team_one') )
    or 'vk' == get_field('social_team_one') ): ?>
    <div class="soc">
       <a href="<?php the_field('link_social_one'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/vk.svg"></a>
    </div>
<?php endif; ?>
<?php if( in_array( 'instagram', get_field('social_team_one') )
    or 'instagram' == get_field('social_team_one') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_one'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/in.svg"></a>
    </div>
<?php endif; ?>
<?php if( in_array( 'behance', get_field('social_team_one') )
    or 'behance' == get_field('social_team_one') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_one'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/be.svg"></a>
    </div>
<?php endif; ?>



<?php if( in_array( 'vk', get_field('social_team_two') )
    or 'vk' == get_field('social_team_two') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_two'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/vk.svg"></a>
    </div>
<?php endif; ?>
<?php if( in_array( 'instagram', get_field('social_team_two') )
    or 'instagram' == get_field('social_team_two') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_two'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/in.svg"></a>
    </div>
<?php endif; ?>
<?php if( in_array( 'behance', get_field('social_team_two') )
    or 'behance' == get_field('social_team_two') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_two'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/be.svg"></a>
    </div>
<?php endif; ?>



<?php if( in_array( 'vk', get_field('social_team_thee') )
    or 'vk' == get_field('social_team_thee') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_thee'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/vk.svg"></a>
    </div>
<?php endif; ?>
<?php if( in_array( 'instagram', get_field('social_team_thee') )
    or 'instagram' == get_field('social_team_thee') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_thee'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/in.svg"></a>
    </div>
<?php endif; ?>
<?php if( in_array( 'behance', get_field('social_team_thee') )
    or 'behance' == get_field('social_team_thee') ): ?>
    <div class="soc">
        <a href="<?php the_field('link_social_thee'); ?>"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/be.svg"></a>
    </div>
<?php endif; ?>
