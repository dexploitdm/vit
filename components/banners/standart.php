<section id="banner" class="height_right_p" style=" background-image:
<?php if( get_field('cover_ban') ): ?>
    url(<?php echo get_field('cover_ban'); ?>);
<?php else: ?>
    url(<?php  echo get_template_directory_uri() ?>/assets/img/banners/landing_page.png);
<?php endif; ?>">
    <div class="layout_banner">
        <div class="content_banner standart_position">
            <p><?php the_title(); ?></p>
            <div class="breadcrumb">
                <a href="<?php echo get_site_url(); ?>/">Главная</a>
                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
                <a href="<?php echo get_site_url(); ?>/"><?php the_terms( $ids, 'tax_serv', ''); ?></a>
                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
                <a class="breadcrumb_active"><?php the_title(); ?></a>
            </div>
            <div class="desc_banner">
                <p><?php the_excerpt(); ?></p>
            </div>
        </div>
        <div class="other_banner">
            <div class="price_time">
                <div class="price_t_title">
                    <p class="title_pt">Стоимость проекта:</p>
                    <p class="value_pt"><?php the_field('price_service'); ?></p>
                    <p class="title_pt">Срок реализации:</p>
                    <p class="value_pt"><?php the_field('time_relise'); ?></p>
                </div>
            </div>
            <a href="<?php echo get_site_url(); ?>/online_request" class="btn_price">
                <div class="body_btn">Расчитать точную стоимость
                    и сроки разработки</div>
            </a>
        </div>
    </div>
</section>