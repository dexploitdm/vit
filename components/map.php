<?php 
$tel_messagers = get_option('number_soc_v'); 
?>
<section class="maps">
    <div class="layout_maps">
        <div class="desc_contact">
            <div class="title_m">
                <p>Контакты</p>
            </div>
            <div class="content_m">
                <div class="sector_text">
                    <p class="value_m">Телефон:</p>
                    <p><?php echo get_option('phone_v'); ?></p>
                </div>
                <div class="sector_text">
                    <p class="value_m">Офис:</p>
                    <p><?php echo get_option('location_v'); ?></p>
                </div>
                <div class="sector_text">
                    <p class="value_m">Режим работы:</p>
                    <p class="time_work"><?php echo get_option('time_works_v'); ?></p>
                </div>
                <div class="sector_text">
                    <p class="value_m">Почта:</p>
                    <a href="mailto:<?php echo get_option('email_v'); ?>"><?php echo get_option('email_v'); ?></a>
                </div>
                <div class="sector_text">
                    <p class="value_m">Вконтакте:</p>
                    <a href="<?php echo get_option('vk_link_v'); ?>" target="_blank"><?php echo get_option('vk_v'); ?></a>
                </div>
                <div class="sector_text">
                    <div class="messenger_items">
                        <p class="value_m">Viber, Whatsapp, Telegram</p>
                        <a href="tel:<?php telRepl($tel_messagers); ?>"><?php echo get_option('number_soc_v'); ?></a>
                    </div>
                    <div class="link_messenger">
                        <a href="tg://resolve?domain=<?php echo get_option('messenger_telegram'); ?>" target="_blank" class="telegram">
                            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/messenger/telegram.svg"">
                        </a>
                        <a href="https://api.whatsapp.com/send?phone=<?php echo get_option('messenger_whatsapp'); ?>" target="_blank" class="whatsapp">
                            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/messenger/whatsapp.svg"">
                        </a>
                        <a href="viber://chat?number=<?php echo get_option('messenger_viber'); ?>" target="_blank" class="viber">
                            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/messenger/viber.svg"">
                        </a>
                    </div>
                </div>
                <div class="sector_social">
                    <div class="soc_item">
                        <a href="<?php echo get_option('soc_vk_v'); ?>" target="_blank">
                            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/vk.svg">
                        </a>
                    </div>
                    <div class="soc_item">
                        <a href="<?php echo get_option('soc_instagram_v'); ?>" target="_blank">
                            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/in.svg">
                        </a>
                    </div>
                    <div class="soc_item">
                        <a href="<?php echo get_option('soc_behance_v'); ?>" target="_blank">
                            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/be.svg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="maps_contact">
            <div id="map"></div>
        </div>
    </div>
</section>
<script>
//Карта яндекс API
ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [58.58439932, 49.65925934],
            zoom: 16,
            controls: []
        }, {suppressMapOpenBlock: true}, {
            searchControlProvider: 'yandex#search'
        }),

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'ул.Октябрьский проспект 120',
           // balloonContent: 'ул.Карла Либкнехта 120, 4-й этаж, офис 403'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '<?php echo get_site_url(); ?>/wp-content/themes/vit/assets/img/other/cordinate.svg',
            // Размеры метки.
            iconImageSize: [50, 92],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-15, -45]
        });

    myMap.geoObjects
        .add(myPlacemark);
});
</script>