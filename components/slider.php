<?php $sliders = new WP_Query(array('post_type' => 'sliders', 'order' => 'ASC')) ?>
<?php if ($sliders->have_posts() ): ?>
<div class="slider">
    <div class="slide_viewer">
        <div class="slide_group">
            <?php while ($sliders->have_posts()) : $sliders->the_post(); ?>
                <div class="slide" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                    <div class="toning">
                        <div class="layout_slide">
                            <div class="slider_title">
                                <h1><?php the_title(); ?></h1>
                            </div>
                            <div class="slider_content">
                                <?php the_content(); ?>
                            </div>
                            <div class="slider_btns">
                                <?php $btnonetext = get_post_meta($post->ID, 'btnonetext', true); ?>
                                <?php  if($btnonetext == ''):  ?>
                                    <a href="#question_form" class="btn_meet question_link">Задать вопрос</a>
                                <?php else: ?>
                                    <a class="btn_meet" href="<?php echo get_post_meta($post->ID, 'btnonelink', true); ?>"><?php echo get_post_meta($post->ID, 'btnonetext', true); ?></a>
                                <?php endif; ?>
                                <?php $btntwotext = get_post_meta($post->ID, 'btntwotext', true); ?>
                                <?php  if($btntwotext == ''):  ?>
                                    <a href="<?php echo get_site_url(); ?>/online_request" class="btn_transparent">Рассчитать стоимость</a>
                                <?php else: ?>
                                    <a class="btn_transparent" href="<?php echo get_post_meta($post->ID, 'btntwolink', true); ?>"><?php echo get_post_meta($post->ID, 'btntwotext', true); ?></a>
                                <?php endif; ?> <div></div>
                            </div>
                        </div>
                    </div>
                </div>
             <?php endwhile; ?>
        </div>
        <div class="directional_nav">
            <div class="previous_btn" title="Previous">
                <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.7132 1.99998L2 11.7132L11.7132 21.4264" stroke="#4DC1FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <div class="slide_buttons"></div>
            <div class="next_btn" title="Next">
                <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.7133 21.4264L21.4265 11.7132L11.7133 1.99998" stroke="#4DC1FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>