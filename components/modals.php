
<div id="question_form" class="mfp-hide white-popup">
    <div class="modalDialog vitmodal">
        <div class="layout_modal">
            <a title="Закрыть" class="close mfp-close">
                <img class="close_modal" src="<?php  echo get_template_directory_uri() ?>/assets/img/other/close_mod.svg">
            </a>
        <div class="body_questions">
            <div class="info_modal">
                <p class="title">Задать вопрос</p>
                <p>Если у вас есть вопросы - Вы можете написать нам. Ответим по телефону или на почту в течение 20 минут.
                    <br>Поля помеченные звездочкой обязательны для заполнения. Обработка заявок происходит пн-пт с 9.00 до 18.00.</p>
            </div>
            <?php
            if ( function_exists('dynamic_sidebar') )
                dynamic_sidebar('modal_questions');
            ?>
            <div class="proccess_data"><p>Нажимая на кнопку Вы даете согласие на обработку <a href="<?php echo get_site_url(); ?>/privacy_policy" target="_blank">персональных данных</a></p></div>
        </div>
        </div>
    </div>
</div>
<div id="order_form" class="mfp-hide white-popup">
    <div class="modalDialog vitmodal">
        <div class="layout_modal">
            <a title="Закрыть" class="close mfp-close">
                <img class="close_modal" src="<?php  echo get_template_directory_uri() ?>/assets/img/other/close_mod.svg">
            </a>
        <div class="body_questions">
            <div class="info_modal">
                <p class="title">Оставьте заявку</p>
           </div>
           <form id="form_order">
                <?php 
                $queried_object = get_queried_object();
                $taxonomy = $queried_object->taxonomy;
                $term_id = $queried_object->term_id;
                $term = get_term( $term_id, $taxonomy ); $name_title = $term->name; ?>
                <?php if($name_title == ''): ?>
                    <input type="hidden" name="current_page" value="<?php the_title(); ?>">
                <?php else: ?>
                    <input type="hidden" name="current_page" value="<?php echo $name_title; ?>">
                <?php endif; ?>
                <input id="result_title" type="hidden" name="service" placeholder="">
                <div class="sector_form">
                    <div class="name_input order_name">
                        <div class="body_input">
                            <p>Ваше имя *</p>
                            <input type="text" name="name" placeholder="Как Вас зовут?">
                        </div>
                    </div>
                    <div class="phone_input order_phone">
                        <div class="body_input">
                            <p>Номер Вашего телефона *</p>
                            <input id="phone_usl" name="phone" type="text" placeholder="8 (___) ___ __ __" class="form-control input-md">
                        </div>
                    </div>
                    <div class="msg_input order_msg">
                        <div class="body_input">
                            <p>Ваше сообщение *</p>
                            <textarea name="message" placeholder="Напишите что Вас интересует"></textarea>
                        </div>
                    </div>
                    <div class="btn_form">
                        <button class="btn_h" href="/">Оставить заявку</button>
                    </div>
                </div>
            </form>
            <div class="proccess_data"><p>Нажимая на кнопку Вы даете согласие на обработку <a href="<?php echo get_site_url(); ?>/privacy_policy" target="_blank">персональных данных</a></p></div>
        </div>
        </div>
    </div>
</div>
<div id="question_post" class="mfp-hide white-popup">
    <div class="layout_modal">
            <a title="Закрыть" class="close mfp-close">
                <img class="close_modal" src="<?php  echo get_template_directory_uri() ?>/assets/img/other/close_mod.svg">
            </a>
        <div class="popup">
            <div class="img_ok">
                    <img src="<?php  echo get_template_directory_uri() ?>/assets/img/post_ok.svg">
            </div>
            <div class="info_modal">
                <p class="title">Вопрос отправлен</p>
                <p>Ваш вопрос отправлен и уже обрабатывается.</p>
            </div>
        </div>
    </div>
</div>
<div id="order_post" class="mfp-hide white-popup">
    <div class="layout_modal">
            <a title="Закрыть" class="close mfp-close">
                <img class="close_modal" src="<?php  echo get_template_directory_uri() ?>/assets/img/other/close_mod.svg">
            </a>
        <div class="popup">
            <div class="img_ok">
                    <img src="<?php  echo get_template_directory_uri() ?>/assets/img/post_ok.svg">
            </div>
            <div class="info_modal">
                <p class="title">Заявка отправлена</p>
                <p>Ваша заявка отправлена и уже обрабатывается.</p>
            </div>
        </div>
    </div>
</div>
