<li class="list_service full_lists"><a href="<?php echo get_site_url(); ?>/sentence">
        <img style="width: 16px" class="icon_menu" src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/menu_service.png">
        Услуги</a>
    <ul class="full_menu">
        <div class="grid_menu">
            <div class="child_grid">
                <?php
                $terms = get_terms(
                    'tax_serv', array(
                    'orderby'    => 'count',
                    'hide_empty' => 0,
                    'order' => 'DESC',
                    //'offset' => 4,
                    'number' => 2
                ) );
                $queried_object = get_queried_object();
                $taxonomy = $queried_object->taxonomy;
                $term_id = $queried_object->term_id;
                foreach( $terms as $term ) {
                $args = array(
                    'post_type' => 'serv',
                    'tax_serv' => $term->slug,
                    'order' => 'ASC'
                );
                $query = new WP_Query( $args );
                $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                <a href="<?php echo $term_link; ?>" class="title_service_menu"><?php echo $term->name ?> </a>
                <ul class="sub_services">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <li id="post-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <?php if( in_array( 'is_new', get_field('new_services') )
                                or 'is_new' == get_field('new_services') ): ?>
                                <p class="new">new</p>
                            <?php endif; ?>
                        </li>
                    <?php endwhile;
                    echo '</ul>';
                    wp_reset_postdata();
                    } ?>
            </div>
            <div class="child_grid">
                <?php
                $terms = get_terms(
                    'tax_serv', array(
                    'orderby'    => 'count',
                    'hide_empty' => 0,
                    'order' => 'DESC',
                    'offset' => 2,
                    'number' => 3
                ) );
                $queried_object = get_queried_object();
                $taxonomy = $queried_object->taxonomy;
                $term_id = $queried_object->term_id;
                foreach( $terms as $term ) {
                $args = array(
                    'post_type' => 'serv',
                    'tax_serv' => $term->slug,
                    'order' => 'ASC'
                );
                $query = new WP_Query( $args );
                $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                <a href="<?php echo $term_link; ?>" class="title_service_menu"><?php echo $term->name ?> </a>
                <ul class="sub_services">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <li id="post-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <?php if( in_array( 'is_new', get_field('new_services') )
                                or 'is_new' == get_field('new_services') ): ?>
                                <p class="new">new</p>
                            <?php endif; ?>
                        </li>
                    <?php endwhile;
                    echo '</ul>';
                    wp_reset_postdata();
                    } ?>
            </div>
            <div class="child_grid">
                <?php
                $countterms = wp_count_terms( 'tax_serv', array() );
                $offset = 5;
                $number = $countterms - $offset;
                $terms = get_terms(
                    'tax_serv', array(
                    'orderby'    => 'count',
                    'hide_empty' => 0,
                    'order' => 'DESC',
                    'offset' => 5,
                    'number' => $number
                ) );
                $queried_object = get_queried_object();
                $taxonomy = $queried_object->taxonomy;
                $term_id = $queried_object->term_id;
                foreach( $terms as $term ) {
                $args = array(
                    'post_type' => 'serv',
                    'tax_serv' => $term->slug,
                    'order' => 'ASC'
                );
                $query = new WP_Query( $args );
                $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                <a href="<?php echo $term_link; ?>" class="title_service_menu"><?php echo $term->name ?> </a>
                <ul class="sub_services">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <li id="post-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <?php if( in_array( 'is_new', get_field('new_services') )
                                or 'is_new' == get_field('new_services') ): ?>
                                <p class="new">new</p>
                            <?php endif; ?>
                        </li>
                    <?php endwhile;
                    echo '</ul>';
                    wp_reset_postdata();
                    } ?>
            </div>
        </div>
    </ul>
</li>