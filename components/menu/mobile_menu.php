<div class="navmob">
    <p class="title">Выберите услугу</p>
    <input type="checkbox" id="hamburger-checkbox"/>
    <label class="hamburger-icon" for="hamburger-checkbox">
        <span>
        <i class="fa fa-angle-down fa-1x" aria-hidden="true"></i>
        </span>
    </label>
    <nav class="nav" role="navigation">
        <ul class="nav__list">
            <?php $terms = get_terms(
                'tax_serv', array(
                'hide_empty' => 0,
                'order' => 'DESC',
            ) );
            $queried_object = get_queried_object();
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            $taxonomy_slug = 'tax_serv';
            foreach( $terms as $term ): ?>
                <?php $args = array(
                    'post_type' => 'serv',
                    'tax_serv' => $term->slug,
                    'order' => 'ASC',
                );
                $query = new WP_Query( $args );
                $id_field = $term->term_id;
                $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                <?php if( in_array( 'yes_menu', get_field('stick_in_menu', $taxonomy_slug . '_' . $id_field) )
                    or 'yes_menu' == get_field('stick_in_menu', $taxonomy_slug . '_' . $id_field) ): ?>
                    <li class="list_service service<?php echo $id_field ?>">
                    <input id="group-<?php echo $id_field ?>" type="checkbox" hidden />
                    <label for="group-<?php echo $id_field ?>" class="title_label"><span class="fa fa-angle-right"></span>
                    <img style="width: 16px" class="icon_menu" src="<?php the_field('img_menu_default', $taxonomy_slug . '_' . $id_field); ?>">
                    <?php echo $term->name ?></label>
                    <ul class="group-list">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <li id="post-<?php the_ID(); ?>">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>
                        <?php endwhile; ?>
                        <li id="">
                                <a href="<?php echo $term_link; ?>">Смотреть все</a>
                        </li>
                    </ul>
                    <?php wp_reset_postdata(); ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="list_service">
                <input id="group-4" type="checkbox" hidden />
                <label for="group-4"  class="title_label"><span class="fa fa-angle-right"></span>
                    <img style="width: 16px" class="icon_menu" src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/menu_service.png">
                    Услуги</label>
                <ul class="group-list">
                    <li>
                        <?php
                        $terms = get_terms(
                            'tax_serv', array(
                            'hide_empty' => 0,
                            'order' => 'DESC',
                        ) );
                        $queried_object = get_queried_object();
                        $taxonomy = $queried_object->taxonomy;
                        $term_id = $queried_object->term_id;
                        $taxonomy_slug = 'tax_serv';
                        foreach( $terms as $term ): ?>
                            <?php $args = array(
                                'post_type' => 'serv',
                                'tax_serv' => $term->slug,
                                'order' => 'ASC',
                            );
                            $query = new WP_Query( $args );
                            $id_field = $term->term_id;
                            $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                            <input id="group-x<?php echo $id_field ?>" type="checkbox" hidden />
                            <label for="group-x<?php echo $id_field ?>" class="title_sub_label"><span class="fa fa-angle-right"></span>
                                <?php echo $term->name ?></label>
                            <ul class="sub-group-list">
                                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                    <li id="post-<?php the_ID(); ?>">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                            <?php wp_reset_postdata(); ?>
                        <?php endforeach; ?>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>