<section class="works">
    <div class="title_h">
        <h2>Последние проекты</h2>
    </div>
    <div id="works">
    <?php
        $idObj = get_category_by_slug('work_all');
        $all = $idObj->term_id;
        $wpb_all_query = new WP_Query(array(
            'cat' => $all,
            'post_type'=>'post',
            'post_status'=>'publish',
            'posts_per_page'=> 20,
            'order' => 'ASC'
        )); ?> 
        <?php if ( $wpb_all_query->have_posts() ) : ?>
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    <?php if( in_array( 'work_in_home', get_field('stick_work_in_home') )
                        or 'work_in_home' == get_field('stick_work_in_home') ): ?>
                        <div class="box">
                            <img src="<?php the_post_thumbnail_url(); ?>" />
                            <div class="bg_desc_work">
                                <div class="body_desc_work">
                                    <div class="title_w_h"><p><?php the_title(); ?></p></div>
                                    <div class="desc_w_h"><?php the_excerpt(); ?></div>
                                    <a href="<?php the_permalink(); ?>">
                                        <button>Смотреть<glyph class="arrow_icon_s"></glyph></button>
                                    </a>
                                </div>
                            </div>
                         </div>
                    <?php else: ?><?php endif; ?>
                <?php endwhile; ?>
                <div class="box">
                    <a href="<?php echo get_site_url(); ?>/portfolio"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/other/7.png" /></a>
                </div>
                <?php wp_reset_postdata(); ?>
        <?php else : ?>
        <?php endif; ?>
    </div>
    <div style="clear: both;"></div>
</section>