<section class="works smob work_service_block">
        <?php
        $idObj = get_category_by_slug('work_all');
        $all = $idObj->term_id;
        $wpb_all_query = new WP_Query(array(
            'cat' => $all,
            'post_type'=>'post',
            'post_status'=>'publish',
            'posts_per_page'=> 20,
            'order' => 'ASC'
        )); ?>
        <?php if ( $wpb_all_query->have_posts() ) : ?>
            <div class="title_h">
                <h2>Последние проекты</h2>
            </div>
            <div class="grid_work">
                <div class="grid-sizer"></div>
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    <?php if( in_array( 'work_in_home', get_field('stick_work_in_home') )
                        or 'work_in_home' == get_field('stick_work_in_home') ): ?>
                        <div class="grid-item">
                            <div class="bg_work_cover" style="background-image:
                                    <?php if( get_field('cover_page_work') ): ?>
                                            url(<?php echo get_field('cover_page_work'); ?>);
                                    <?php else: ?>
                                            url(<?php echo get_the_post_thumbnail_url(); ?>);
                                    <?php endif; ?>">
                                <div class="bg_desc_work">
                                    <div class="body_desc_work">
                                        <div class="title_w_h"><p><?php the_title(); ?></p></div>
                                        <div class="desc_w_h"><p><?php the_excerpt(); ?></p></div>
                                        <a href="<?php the_permalink(); ?>">
                                            <button>Смотреть<glyph class="arrow_icon_s"></glyph></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?><?php endif; ?>
                <?php endwhile; ?>
                    <div class="grid-item">
                        <a href="<?php echo get_site_url(); ?>/portfolio">
                            <div class="bg_work_cover" style="background-image: url(<?php  echo get_template_directory_uri() ?>/assets/img/other/7.png);"></div>
                        </a>
                    </div>
                <?php wp_reset_postdata(); ?>
            </div>
        <?php else : ?>
        <?php endif; ?>
    </section>