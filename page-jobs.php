<?php
/*
Template Name: Вакансии
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="bg_team" style=" background-image: url(<?php the_post_thumbnail_url(); ?>);">
        <div class="top_info">
            <div class="title_info">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="breadcrumb">
                <a href="<?php echo get_site_url(); ?>/">Главная</a>
                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
                <a class="active">Вакансии</a>
            </div>
            <div class="desc_team jobs_desc">
                <?php the_field('desc_jobs_page'); ?>
            </div>
        </div>
    </div>
<?php endwhile; wp_reset_query(); ?>
<section class="page_jobs">
<?php $jobs = new WP_Query(array('post_type' => 'jobs', 'order' => 'ASC')) ?>
<?php if ($jobs->have_posts() ): ?>
    <div class="layout_jobs">
        <div class="gridjobs">
    <?php while ($jobs->have_posts()) : $jobs->the_post(); ?>
            <div class="single_job">
                <div class="job_title">
                    <p><?php the_title(); ?></p>
                    <span class="date_job"><?php echo get_the_date('j F Y'); ?></span>
                </div>
                <div class="job_desc">
                    <?php the_excerpt(); ?>
                </div>
                <div class="job_btn">
                    <a href="<?php the_permalink(); ?>">
                        <button class="job"><p>Подробнее</p></button>
                    </a>
                </div>
            </div>
    <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
</section>
<?php get_footer(); ?>