<?php
/*
Template Name: Отзывы наших клиентов
Template Post Type: page
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <div class="top_info">
        <div class="title_info">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="breadcrumb">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="breadcrumb_active">Отзывы</a>
        </div>
    </div>
    <div class="links_btns">
        <a href="<?php echo get_site_url(); ?>/thanks""><button class="btn_h">Благодарности</button></a>
        <a href="https://vk.com/topic-34687056_31455682" target="_blank"><button class="btn_vk"><glyph class="vk_icon"></glyph>Оставить отзыв</button></a>
    </div>
<div class="reviews">
<?php
    $access_token = 'cde1757151a668fa95f025469c33c0d2e6b76335b8f6f308938592c517e5f5023830a2aec6430132fee80';
    $service_key = '0628646b0628646b0606f74e20064b4079006280628646b5a2d365b096a77addbfafdea';
    $group_id = '34687056';
    $topic_id = '31455682';
    $request_params = array( //подготавливаем параметры для запроса к api vk
        'group_id' => $group_id,
        'access_token' => $access_token,
        'topic_id' => $topic_id,
        'sort' => 'DESC',
        'count' => 99,
        'extended' => 1,
        'v' => '5.73'
    );
    $url = 'https://api.vk.com/method/board.getComments';
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_POST => true, // это именно POST запрос!
        CURLOPT_RETURNTRANSFER => true, // вернуть ответ ВК в переменную
        CURLOPT_SSL_VERIFYPEER => false, // не проверять https сертификаты
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_POSTFIELDS => $request_params,
        CURLOPT_URL => $url, // веб адрес запроса
    ));
    $result = curl_exec($ch); // запрос выполняется и всё возвращает в переменную
    curl_close($ch);
    $result = json_decode($result);
    foreach ($result->response->profiles as $item) {
        $array_profiles[$item->id]['name'] = $item->first_name . ' ' . $item->last_name;
        $array_profiles[$item->id]['photo'] = $item->photo_100;
    }
    $index = 0;
    foreach ($result->response->items as $item) {
        if (!(($item->from_id == -$group_id) || ($item->from_id == 21780408))) {
            $review[$index]['id'] = $item->from_id;
            $review[$index]['name'] = $array_profiles[$item->from_id]['name'];
            $review[$index]['photo'] = $array_profiles[$item->from_id]['photo'];
            $review[$index]['text'] = $item->text;
            $review[$index]['date'] = $item->date;
            $review[$index]['post_id'] = $item->id;
            $index_attach = 0;
            $attachment_type = true;
            if (isset($item->attachments)) {
                foreach ($item->attachments as $attachment) {
                    if ($attachment->type == 'photo') {
                        $attachment_type = false;
                    };
                }
            }
            if (!$attachment_type || $review[$index]['text'] == '') {
                unset($review[$index]);
            } else {
                $index++;
            }

        }
    }
    ?>
<div class="layout_reviews">
    <?php 
    $total = count($review);
    $counter = 0; ?>
    <?php foreach ($review as $item) { ?>
    <?php $counter++; ?>
        <?php if($counter == $total): ?><?php else: ?>
        <div class="single_rev">
                <div class="body_rev">
                    <div class="avatar_r">
                        <div class="ava">
                            <img class="avatar_user" src="<?= $item['photo'] ?>"  alt="<?= $item['name'] ?>">
                            <a class="vk_link_user" href="https://vk.com/id<?= $item['id'] ?>" target="_blank">
                                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/vk_link.svg">
                            </a>
                        </div>
                    </div>
                    <div class="info_user_r">
                        <div class="name_user"><a href="https://vk.com/topic-34687056_31455682?post=<?= $item['post_id'] ?>"><?= $item['name'] ?></a></div>
                        <p class="date_r"><?php echo date_i18n('d F Y', $item['date']); ?></p>
                    </div>
                   <div class="comment_review">
                        <div class="body_com">
                            <p>
                            <?php $text = $item['text'];
                                $result_text = preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<noindex><a href="$1://$2" target="_blank" rel="nofollow">$1://$2</a></noindex>$3', $text);
                                echo $result_text; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
        <?php endif; ?>
    <?php } ?>
    </div>
</div>
<?php endwhile; wp_reset_query(); ?>
<?php get_template_part( 'components/map'); ?>
<?php get_footer(); ?>