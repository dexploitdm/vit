<?php get_header(); ?>
<style>header { position: inherit;}</style>
<?php while( have_posts() ) : the_post(); ?>
<div class="top_info">
   <div class="title_info">
       <h1><?php the_title(); ?></h1>
   </div>
    <div class="breadcrumb">
        <a href="<?php echo get_site_url(); ?>/">Главная</a>
        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
        <a href="<?php echo get_site_url(); ?>/portfolio">Портфолио</a>
        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
        <a class="breadcrumb_active"><?php the_title(); ?></a>
    </div>
</div>
<section class="work_content">
    <div class="about_work">
        <div id="st" class="body_about">
            <div class="title_about">
                <p>О проекте</p>
            </div>
            <div class="content_about_work">
                <?php the_field('about_work'); ?>
            </div>
            <?php if( get_field('link_full_work') ): ?>
            <div class="button_work">
                <noindex>
                <a href="http://<?php the_field('link_full_work'); ?>" rel="nofollow" target="_blank">
                    <button class="work_link"><p><?php the_field('link_full_work'); ?></p></button>
                </a>
                </noindex>
            </div><?php endif; ?>
            <div class="soc">
                <script type="text/javascript">(function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();</script>
                <div class="pluso" data-background="#ebebeb" data-options="small,square,line,horizontal,counter,theme=08" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
            </div>
            <div class="info">
                <p>Что бы заказать сайт - позвоните нам по телефону: +7 (8332) 411-451
                    или оставьте заявку</p>
            </div>
            <div class="btn_work_order">
                <button class="btn_standart order_link" href="#order_form">Оставить заявку</button>
            </div>
        </div>
    </div>
    <div class="content_image">
        <?php the_content(); ?>
    </div>
</section>
<?php endwhile; wp_reset_query(); ?>
<script>
(function(){
    var a = document.querySelector('#st'), b = null, P = 20;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом
    window.addEventListener('scroll', Ascroll, false);
    document.body.addEventListener('scroll', Ascroll, false);
    function Ascroll() {
    if (b == null) {
        var Sa = getComputedStyle(a, ''), s = '';
        for (var i = 0; i < Sa.length; i++) {
        if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0) {
            s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
        }
        }
        b = document.createElement('div');
        b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
        a.insertBefore(b, a.firstChild);
        var l = a.childNodes.length;
        for (var i = 1; i < l; i++) {
        b.appendChild(a.childNodes[1]);
        }
        a.style.height = b.getBoundingClientRect().height + 'px';
        a.style.padding = '0';
        a.style.border = '0';
    }
    var Ra = a.getBoundingClientRect(),
        R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.maps').getBoundingClientRect().top + 20);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
    if ((Ra.top - P) <= 0) {
        if ((Ra.top - P) <= R) {
        b.className = 'stop';
        b.style.top = - R +'px';
        } else {
        b.className = 'sticky';
        b.style.top = P + 'px';
        }
    } else {
        b.className = '';
        b.style.top = '';
    }
    window.addEventListener('resize', function() {
        a.children[0].style.width = getComputedStyle(a, '').width
    }, false);
    }
})()
</script>
<stopen></stopen>
<div class="clear_content"></div>
<?php get_template_part( 'components/map'); ?>
<?php get_footer(); ?>