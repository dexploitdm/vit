<?php get_header(); ?>
<div class="top_info type">
    <div class="title_info">
        <h1>404</h1>
    </div>
    <div class="breadcrumb top">
        <a href="<?php echo get_site_url(); ?>/">Главная</a>
        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
        <a class="breadcrumb_active">404</a>
    </div>
    <div style="padding-bottom: 40px"></div>
</div>
<div class="pagemaps">
    <?php get_template_part( 'components/map'); ?>
</div>
<?php get_footer(); ?>