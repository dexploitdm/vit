<?php
/*
Template Name: ТипСтр/Услуга | Разработка сайтов
Template Post Type: serv
*/
get_header(); ?>
<?php
$title_page_serv = get_the_title();
?>
<?php while( have_posts() ) : the_post(); ?>
    <?php if( in_array( 'right', get_field('position_desc_banner') )
        or 'right' == get_field('position_desc_banner') ): ?>
        <?php get_template_part( 'components/banners/right'); ?>
    <?php else: ?>
        <?php get_template_part( 'components/banners/standart'); ?>
    <?php endif; ?>
<section class="block_page on_work">
<?php if( get_field("in_enter_project") ): ?>
    <div class="title_h">
        <p>Что входит в проект</p>
    </div>
    <div class="in_project">
	    <?php the_field( "in_enter_project" ); ?>
    </div>
    <?php else :?>
    <style>.block_page {padding-bottom: 0 !important;}</style>
    <?php endif; ?>
</section>
<section class="work_service_block page_service">
    <?php
    $idObj = get_category_by_slug('portfolio');
    $all = $idObj->term_id;
    $wpb_all_query = new WP_Query(array(
        'cat' => $all,
        'post_type'=>'post',
        'post_status'=>'publish',
    )); ?>
    <?php if ( $wpb_all_query->have_posts() ) : ?>
            <div class="title_h">
                <p>Примеры работ</p>
            </div>
            <div class="grid_work">
                <div class="grid-sizer"></div>
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    <?php $title = get_the_title();
                    $image_url = get_the_post_thumbnail_url();
                    $excerpt = get_the_excerpt();
                    $id_post = get_the_ID();
                    $taxonomy = 'category';
                    $cover = get_field('cover_page_work', $id_post);
                    $link = get_the_permalink(); ?>
                    <?php  $post_object = get_field('select_services');
                    if( $post_object ):
                        $post = $post_object;
                        setup_postdata( $post );  ?>
                        <?php $title_select_serv = get_the_title(); ?>
                        <?php wp_reset_postdata();  ?>
                    <?php endif; ?>
                    <?php if($title_select_serv == $title_page_serv): ?>
                        <div class="grid-item">
                            <div class="bg_work_cover" style="background-image:
                                    <?php if( $cover ): ?>
                                            url(<?php echo $cover; ?>);
                                    <?php else: ?>
                                            url(<?php echo $image_url; ?>);
                                    <?php endif; ?>">
                                <div class="bg_desc_work">
                                    <div class="body_desc_work">
                                        <div class="title_w_h"><p><?php  echo $title; ?></p></div>
                                        <div class="desc_w_h"><p><?php  echo $excerpt; ?></p></div>
                                        <a href="<?php  echo $link; ?>">
                                            <button>Смотреть<glyph class="arrow_icon_s"></glyph></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
    <?php else : ?>
    <?php endif; ?>
</section>
<section class="content_type_page">
    <?php the_content(); ?>
</section>
<?php get_template_part( 'components/map'); ?>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>