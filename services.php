<?php
/*
Template Name: Услуги
Template Post Type: page
*/
get_header(); ?>
<div class="sector_page sentence">
    <div class="title_page">
        <h1>Все услуги</h1>
        <div class="breadcrumb">
            <a href="<?php echo get_site_url(); ?>/">Главная</a>
            <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
            <a class="breadcrumb_active">Услуги</a>
        </div>
    </div>
    <div class="body_services">
        <div class="column_service">
            <?php
            $terms = get_terms(
                'tax_serv', array(
                'hide_empty' => 0,
                'slug'=> array('dev_site','analitics','webfull'),
                'order' => 'DESC',
            ) );
            $queried_object = get_queried_object();
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            foreach( $terms as $term ) {
            $args = array(
                'post_type' => 'serv',
                'tax_serv' => $term->slug,
                'order' => 'ASC'
            );
            $query = new WP_Query( $args );
            $term_link = get_term_link($term->term_id, $taxonomy);  ?>
            <a href="<?php echo $term_link; ?>" class="title_service"><?php echo $term->name ?> </a>
            <ul>
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li id="post-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <?php if( in_array( 'is_new', get_field('new_services') )
                            or 'is_new' == get_field('new_services') ): ?>
                            <p class="new">new</p>
                        <?php endif; ?>
                    </li>
                <?php endwhile;
                echo '</ul>';
                wp_reset_postdata();
                } ?>
        </div>
        <div class="column_service">
            <?php
            $terms = get_terms(
                'tax_serv', array(
                'slug'=> array('shops','design','crm'),
                'hide_empty' => 0,
                'order' => 'DESC',
            ) );
            $queried_object = get_queried_object();
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            foreach( $terms as $term ) {
            $args = array(
                'post_type' => 'serv',
                'tax_serv' => $term->slug,
                'order' => 'ASC'
            );
            $query = new WP_Query( $args );
            $term_link = get_term_link($term->term_id, $taxonomy);  ?>
            <a href="<?php echo $term_link; ?>" class="title_service"><?php echo $term->name ?> </a>
            <ul>
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li id="post-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <?php if( in_array( 'is_new', get_field('new_services') )
                            or 'is_new' == get_field('new_services') ): ?>
                            <p class="new">new</p>
                        <?php endif; ?>
                    </li>
                <?php endwhile;
                echo '</ul>';
                wp_reset_postdata();
                } ?>
        </div>
        <div class="column_service">
            <?php
            $terms = get_terms(
                'tax_serv', array(
                'hide_empty' => 0,
                'slug'=> array('question_serv','setting_s'),
                'order' => 'DESC',
            ) );
            $queried_object = get_queried_object();
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            foreach( $terms as $term ) {
            $args = array(
                'post_type' => 'serv',
                'tax_serv' => $term->slug,
                'order' => 'ASC'
            );
            $query = new WP_Query( $args );
            $term_link = get_term_link($term->term_id, $taxonomy);  ?>
            <a href="<?php echo $term_link; ?>" class="title_service"><?php echo $term->name ?> </a>
            <ul>
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li id="post-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <?php if( in_array( 'is_new', get_field('new_services') )
                            or 'is_new' == get_field('new_services') ): ?>
                            <p class="new">new</p>
                        <?php endif; ?>
                    </li>
                <?php endwhile;
                echo '</ul>';
                wp_reset_postdata();
                } ?>
            <?php
            //Вывод остальных
            $countterms = wp_count_terms( 'tax_serv', array() );
            $offset = 8;
            $number = $countterms - $offset;
            $terms = get_terms(
                'tax_serv', array(
                //'orderby'    => 'count',
                'hide_empty' => 0,
                'order' => 'DESC',
                'offset' => 7,
                'number' => $number
            ) );
            $queried_object = get_queried_object();
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id; ?>
            <?php if($countterms > $offset): ?>
            <?php
            foreach( $terms as $term ) {
                $args = array(
                    'post_type' => 'serv',
                    'tax_serv' => $term->slug,
                    'order' => 'ASC'
                );
                $query = new WP_Query( $args );
                $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                <a href="<?php echo $term_link; ?>" class="title_service"><?php echo $term->name ?> </a>
                <ul>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <li id="post-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <?php if( in_array( 'is_new', get_field('new_services') )
                                or 'is_new' == get_field('new_services') ): ?>
                                <p class="new">new</p>
                            <?php endif; ?>
                        </li>
                    <?php endwhile;
                    echo '</ul>';
                    wp_reset_postdata();
            } ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_template_part( 'components/map'); ?>
<?php get_footer(); ?>
 