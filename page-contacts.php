<?php
/*
Template Name: Контакты
Template Post Type: page
*/
get_header(); ?>
<?php
$tel_messagers = get_option('number_soc_v'); 
?>
<?php while( have_posts() ) : the_post(); ?>
<div class="top_info">
    <div class="title_info">
        <h1><?php the_title(); ?></h1>
    </div>
    <div class="breadcrumb">
        <a href="<?php echo get_site_url(); ?>/">Главная</a>
        <img src="<?php  echo get_template_directory_uri() ?>/assets/img/arrow_btn.svg">
        <a class="breadcrumb_active">Контакты</a>
    </div>
</div>
<section class="page_contact">
    <div class="layout_contact">
        <div class="page_els">
            <div class="el">
                <div class="left_sec">
                    <div class="tel">
                        <p class="label_sec">Телефон:</p>
                        <p class="val_sec"> <?php echo get_option('phone_v'); ?></p>
                    </div>
                    <div class="email">
                        <p class="label_sec">Почта:</p>
                        <a class="link_sec" href="mailto:<?php echo get_option('email_v'); ?>"><?php echo get_option('email_v'); ?></a>
                    </div>
                    <div class="social_sec">
                        <div class="soc_sec">
                            <a href="<?php echo get_option('soc_vk_v'); ?>" target="_blank"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/vk.svg"></a>
                        </div>
                        <div class="soc_sec">
                            <a href="<?php echo get_option('soc_instagram_v'); ?>" target="_blank"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/in.svg"></a>
                        </div>
                        <div class="soc_sec">
                            <a href="<?php echo get_option('soc_behance_v'); ?>" target="_blank"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/social/be.svg"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="el">
                <div class="center_sec">
                    <div class="time_work">
                        <p class="label_sec">Режим работы:</p>
                        <p class="val_sec"> <?php echo get_option('time_works_v'); ?></p>
                    </div>
                    <div class="location">
                        <p class="label_sec">Офис:</p>
                        <p class="val_sec"><?php echo get_option('location_v'); ?></p>
                    </div>
                </div>
            </div>
            <div class="el">
                <div class="right_sec">
                    <div class="vk_sec">
                        <p class="label_sec">Вконтакте:</p>
                        <a class="link_sec" href="<?php echo get_option('vk_link_v'); ?>" target="_blank"><?php echo get_option('vk_v'); ?></a>
                    </div>
                    <div class="service_sec">
                        <div class="messenger_items">
                            <p class="label_sec">Viber, Whatsapp, Telegram:</p>
                            <a class="link_sec" href="tel:<?php telRepl($tel_messagers); ?>"><?php echo get_option('number_soc_v'); ?></a>
                        </div>
                        <div class="link_messenger">
                            <a href="tg://resolve?domain=<?php echo get_option('messenger_telegram'); ?>" target="_blank" class="telegram">
                                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/messenger/telegram.svg"">
                            </a>
                            <a href="https://api.whatsapp.com/send?phone=<?php echo get_option('messenger_whatsapp'); ?>" target="_blank" class="whatsapp">
                                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/messenger/whatsapp.svg"">
                            </a>
                            <a href="viber://chat?number=<?php echo get_option('messenger_viber'); ?>" target="_blank" class="viber">
                                <img src="<?php  echo get_template_directory_uri() ?>/assets/img/icons/messenger/viber.svg"">
                            </a>
                        </div>
                    </div>
                    <div class="requisites">
                        <a class="link_r" href="<?php echo get_option('rec_v'); ?>">Скачать реквизиты</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map_full">
    <div id="mapfull"></div>
</section>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>
<script>
//Карта на странице - контакты
ymaps.ready(function () {
    var myMapFull = new ymaps.Map('mapfull', {
            center: [58.58439932, 49.65925934],
            zoom: 16,
            controls: []
        }, {suppressMapOpenBlock: true}, {
            searchControlProvider: 'yandex#search'
        }),

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        myPlacemarkFull = new ymaps.Placemark(myMapFull.getCenter(), {
            draggable: true,
            hintContent: 'ул.Октябрьский проспект 120',
           // balloonContent: 'ул.Карла Либкнехта 120, 4-й этаж, офис 403'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '<?php echo get_site_url(); ?>/wp-content/themes/vit/assets/img/other/cordinate.svg',
            // Размеры метки.
            iconImageSize: [50, 92],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-15, -45]
        });   
    //     .add(new ymaps.Placemark([58.58405932, 49.65945934], {
    //         balloonContent: 'ул.Октябрьский проспект 120, 5-й этаж, офис 513',
    //         iconCaption: 'ул. Октябрьский проспект 120'
    //     }, {
    //         preset: 'islands#blueCircleDotIconWithCaption',
    // }))
    myMapFull.geoObjects.add(myPlacemarkFull);
});
</script>