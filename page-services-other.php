<?php
/*
Template Name: Простые услуги
Template Post Type: serv
*/
get_header(); ?>
<?php while( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'components/banners/other'); ?>
    <section class="content_type_page">
        <?php the_content(); ?>
    </section>
<?php get_template_part( 'components/map'); ?>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>