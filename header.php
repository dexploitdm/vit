<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <title><?php wp_title('', true, 'left'); ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="author" content="dexploitdm">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="<?php  echo get_template_directory_uri() ?>/assets/css/app.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo get_template_directory_uri() ?>/assets/plugins/magnific/magnific-popup.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <?php  wp_head(); ?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=97173e1f-9918-4d5d-b541-37294b48052f" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/assets/plugins/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/assets/plugins/owlcarousel/owl.theme.default.min.css">

    <script src="<?php  echo get_template_directory_uri() ?>/assets/plugins/owlcarousel/as_owl.carousel.min.js"></script>
    <script src="<?php  echo get_template_directory_uri() ?>/assets/plugins/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?php  echo get_template_directory_uri() ?>/assets/js/masonry.js"></script>
    <script src="<?php  echo get_template_directory_uri() ?>/assets/js/imagesloaded.min.js"></script>
</head>
<body>
<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_NONE);
$tel = get_option('phone_v'); 
?>
<header>
<div class="navigation">
    <div class="panel">
        <div class="brand_logo">
            <a href="<?php echo get_site_url(); ?>/"><img src="<?php  echo get_template_directory_uri() ?>/assets/img/logo.svg"></a>
            <div class="brand_header">
                <a href="<?php echo get_site_url(); ?>/">
                    <div class="text_a"><p>Вятка IT</p></div>
                    <div class="text_b"><p class="blue">web</p><p>-студия</p> </div>
                </a>
            </div>
        </div>
        <div id="nav_one">
            <div class="menu">
                <?php wp_nav_menu(array(
                    'theme_location'  => 'nav_header_one',
                    'container' => '',
                    'menu_class' => 'mainnav-flex-container',
                )); ?>
            </div>
        </div>
        <div class="phone">
            <a href="tel:<?php telRepl($tel); ?>"><?php echo get_option('phone_v'); ?></a>
            <a class="mail" href="mailto:<?php echo get_option('email_v'); ?>"><?php echo get_option('email_v'); ?></a>
        </div>
        <div class="question_btn">
            <a href="#question_form" class="question_link">Задать вопрос</a>
        </div>
    </div>
</div>
<?php get_template_part( 'components/menu/mobile_menu'); ?>
<div class="panel_full">
    <div id="nav_two">
        <div class="navigate_service">
            <ul class="">
                <?php
                $terms = get_terms(
                    'tax_serv', array(
                    'hide_empty' => 0,
                    'order' => 'DESC',
                ) );
                $queried_object = get_queried_object();
                $taxonomy = $queried_object->taxonomy;
                $term_id = $queried_object->term_id;
                $taxonomy_slug = 'tax_serv';
                foreach( $terms as $term ): ?>
                    <?php $args = array(
                        'post_type' => 'serv',
                        'tax_serv' => $term->slug,
                        'order' => 'ASC',
                    );
                    $query = new WP_Query( $args );
                    $id_field = $term->term_id;
                    $term_link = get_term_link($term->term_id, $taxonomy);  ?>
                    <?php if( in_array( 'yes_menu', get_field('stick_in_menu', $taxonomy_slug . '_' . $id_field) )
                        or 'yes_menu' == get_field('stick_in_menu', $taxonomy_slug . '_' . $id_field) ): ?>
                        <?php if( in_array( 'yes_two_colums', get_field('colums_two_menu', $taxonomy_slug . '_' . $id_field) )
                            or 'yes_two_colums' == get_field('colums_two_menu', $taxonomy_slug . '_' . $id_field) ): ?>
                            <li class="list_service desktop-nav-list yes_two_colums service<?php echo $id_field ?>">
                        <?php else: ?>
                            <li class="list_service desktop-nav-list service<?php echo $id_field ?>">
                        <?php endif; ?>
                        <?php if( in_array( 'non_lists_menu', get_field('non_lists_in_menu', $taxonomy_slug . '_' . $id_field) )
                            or 'non_lists_menu' == get_field('non_lists_in_menu', $taxonomy_slug . '_' . $id_field) ): ?>
                            <style>.service<?php echo $id_field ?> ul.child_two {display: none !important;}
                            .service<?php echo $id_field ?>:after {display: none !important;}</style>
                        <?php endif; ?>
                        <a href="<?php echo $term_link; ?>" class="">
                            <img style="width: 16px" class="icon_menu" src="<?php the_field('img_menu_default', $taxonomy_slug . '_' . $id_field); ?>">
                            <?php echo $term->name ?> </a>
                        <ul class="child_two mega-sub-menu">
                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                <li id="post-<?php the_ID(); ?>">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php wp_reset_postdata(); ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php get_template_part( 'components/menu/full_menu'); ?>
            </ul>
        </div>
    </div>
</div>
</header>
<main>